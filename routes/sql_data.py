#!./venv/bin/python 
"""API for operating data from sql database.

Filename: sql_data.py
Author: George Pang
Contact: panglilimcse@139.com
"""

import os
import sys
import pandas as pd
from fastapi import APIRouter
from fastapi.params import Depends
from jupyter_client.session import Session

from sql_app.database import get_db, SessionLocal

if __name__ == '__main__':
    sys.path.append(os.getcwd())
    sys.path.append(os.path.dirname(os.getcwd()))

from commons import transform_to_datarows
from classes.MyLogger import MyLog
from classes.Indecies import KDJ, MACD
from classes.commons import KLineType
from sql_app.routes.klines import create_history_kline
from sql_app.routes.klines import read_history_kline
from sql_app.routes.sql import get_codes_from_history_klines

logger = MyLog('sql_data').logger

router = APIRouter(
    prefix="/sql_data",
    tags=["sql_data"],
    #    dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Not found"}},
)


@router.get("/")
async def read_items():
    """The root of get sql_data.

    Returns:
        A string
    """
    return "data from sql_api"


@router.get("/history_kline_codes")
def get_codes_of_klines(db: SessionLocal =Depends(get_db)):
    """Fetch codes list of all stocks from sql."""
    return get_codes_from_history_klines(db)


@router.get("/history_kline/{code}/")
def read_history_kline_sql(
        code: str,
        kind: str = 'K_DAY',
        db:SessionLocal = Depends(get_db)
):
    """Read history k line data from sql database.

    Retrieves rows pertaining to the given keys from the Table instance
    represented by table_handle.  String keys will be UTF-8 encoded.

    Args:
        db:
        code: code.
        kind: A string.

    Returns:
        A dict.
    """
    logger.info(f"{kind}: {code}  sql_data/history_kline".ljust(60, '='))
    data = read_history_kline(code, kind, db)
    if len(data) == 0:
        return None

    result = {
        "title": {"text": code + ":" + kind, "left": 10},
        "columns": list(data[0].keys()),
        "values": list(map(lambda x: list(x.values()), data))
    }

    return result


@router.get('/kdj_macd/{code}')
def get_kdj_macd(code: str,
                 kltype: str = 'K_DAY',
                 db: SessionLocal = Depends(get_db)):
    """Fetches kline data with KDJ and MACD.

    Retrieves k line data form the Table instance
    calculate and intigrate KDJ and MACD data in it.

    Args:
        db:
        code: code of a stock.
        kltype: kltype, such as 'K_DAY'.

    Returns:
        A list of data includeing kdj and macd.

    Raises:
        None
    """
    print(f'get kdj macd of {code},{kltype}, {db}'.center(60, '='))
    result = read_history_kline_sql(code, kltype, db)
    if result is None:
        return pd.DataFrame()

    df = pd.DataFrame(result['values'], columns=result['columns']).fillna(0)
    kdj = KDJ().calculate_kdj(df).fillna(0)
    macd = MACD().get_macd(kdj).fillna(0)

    return macd


@router.post("/create_history_kline/kdj_macd/{code}")
def save_kdj_macd(code: str,
                  kltype: str = 'K_DAY',
                  db: SessionLocal = Depends(get_db)
                  ):
    """Save kdj, macd ro database.

    Args:
        db:
        code: code od a stock.
        kltype: k line type of period.

    Returns:
        None

    """
    print(f'add_kdj_macd_history_klines_sql {kltype}: {code}, {db}'.center(60, '='))

    data = get_kdj_macd(code,kltype, db)
    if data.empty:
        return

    create_history_kline(transform_to_datarows(data), kltype, True)


@router.post("/create_history_kline/kdj_macd")
def save_kdj_macds(db: SessionLocal = Depends(get_db)):
    """Fetches and save KDJ & MACD data from a HistoryKline table.

    Retrieves all codes of stock and add KDJ & MACD for them.

    """
    print('add multi kdj macds of history_klines_sql'.center(60, '='))
    print(f'add multi kdj macds of history_klines_sql, {db}'.center(60, '='))

    for kltype in KLineType:
        if kltype == '':
            continue

        codes = get_codes_from_history_klines(kltype, db)
        print(f'codes === {codes}'.rjust(80, '='))

        for code in codes:
            print(f'kltype: {kltype}, code: {code[0]}'.center(80, '-'))
            data = get_kdj_macd(code[0], kltype, db)
            if data.empty:
                continue
            create_history_kline(transform_to_datarows(data), kltype, True)

