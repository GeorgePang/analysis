#!./venv/bin/python 
"""API for operating data from sql database.

Filename: sql_data.py
Author: George Pang
Contact: panglilimcse@139.com
"""

from fastapi import APIRouter, Depends, HTTPException

# import sys
# sys.path.append("..")
from dependencies.dependencies import get_token_header

router = APIRouter(
    prefix="/items",
    tags=["items"],
    dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Not found"}},
)

fake_items_db = {"plumbus": {"name": "Plumbus"}, "gun": {"name": "Portal Gun"}}


@router.get("/")
async def read_items():
    """Fetches rows from a Smalltable.

    Retrieves rows pertaining to the given keys from the Table instance
    represented by table_handle.  String keys will be UTF-8 encoded.

    Args:
        table_handle: An open smalltable.Table instance.
        keys: A sequence of strings representing the key of each table
          row to fetch.  String keys will be UTF-8 encoded.
        require_all_keys: If True only rows with values set for all keys will be
          returned.

    Returns:
        A dict mapping keys to the corresponding table row data
        fetched. Each row is represented as a tuple of strings. For
        example:

        {b'Serak': ('Rigel VII', 'Preparer'),
         b'Zim': ('Irk', 'Invader'),
         b'Lrrr': ('Omicron Persei 8', 'Emperor')}

        Returned keys are always bytes.  If a key from the keys argument is
        missing from the dictionary, then that row was not found in the
        table (and require_all_keys must have been False).

    Raises:
        IOError: An error occurred accessing the smalltable.
    """
    return fake_items_db


@router.get("/{item_id}")
async def read_item(item_id: str):
    """Read item content.

    Retrieves rows pertaining to the given keys from the Table instance
    represented by table_handle.  String keys will be UTF-8 encoded.

    Args:
        item_id: A tring.

    Returns:
        A dict.

    Raises:
        None
    """
    if item_id not in fake_items_db:
        raise HTTPException(status_code=404, detail="Item not Found")
    return {"name": fake_items_db[item_id]["name"], "item_id": item_id}


@router.put(
    "/{item_id}",
    tags=["custom"],
    responses={403: {"description": "Operatiuon forbidden"}},
)
async def update_item(item_id: str):
    """Update item content.

    Retrieves rows pertaining to the given keys from the Table instance
    represented by table_handle.  String keys will be UTF-8 encoded.

    Args:
        item_id: An open smalltable.Table instance.

    Returns:
        A dict.

    Raises:
        None
    """
    if item_id != "plumbus":
        raise HTTPException(
            status_code=403,
            detail="You can only update the item plumbus",
        )
    return {"item_id": item_id, "name": "The great Plumbus"}

