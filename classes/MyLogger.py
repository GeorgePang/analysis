#!./venv/bin/python 
"""API to.

Filename: MyLogger.py
Author: George Pang
Contact: panglilimcse@139.com
"""
import os
import sys
from  datetime import datetime
from  time import strftime
import logging
# 输出日志路径
PATH = os.path.abspath('.') + '/logs/'
# 设置日志格式#和时间格式
FMT = '%(asctime)s %(filename)s [line:%(lineno)d] %(levelname)s: %(message)s'
DATEFMT = '%Y-%m-%d %H:%M:%S'

class MyLog:
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    def __init__(self, filename):
        """Inite the class.
    
        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            self: self.
            filename: log filename.

        Returns:
            A class instance.

        Raises:
            None
        """
        self.logger = logging.getLogger()
        self.formatter = logging.Formatter(fmt=FMT, datefmt=DATEFMT)
        self.log_filename = '{}{}{}.log'.format(
            PATH, 
            filename, 
            strftime("%Y-%m-%d")
        )
        self.log_filename = f'{PATH}{filename}.log'

#        sh = logging.StreamHandler()#往屏幕上输出
#        sh.setFormatter(format_str) #设置屏幕上显示的格式
#        th = self.RotatingFileHandler(
#            filename=self.log_filename,
#            maxBytes=100*1024*1024,
#            backupCount=10,
#            encoding='utf-8'
#        )#往文件里写入
        #指定间隔时间自动生成文件的处理器
        #实例化TimedRotatingFileHandler
        #interval是时间间隔，backupCount是备份文件的个数，如果超过这个个数，就会自动删除，when是间隔的时间单位，单位有以下几种：
        # S 秒
        # M 分
        # H 小时、
        # D 天、
        # W 每星期（interval==0时代表星期一）
        # midnight 每天凌晨
#        th.setFormatter(format_str)#设置文件里写入的格式
#        self.logger.addHandler(sh) #把对象加到logger里
#        self.logger.addHandler(th)

        self.logger.addHandler(self.get_file_handler(self.log_filename))
#        self.logger.addHandler(self.get_console_handler())
        # 设置日志的默认级别
        self.logger.setLevel(logging.DEBUG)

    # 输出到文件handler的函数定义
    def get_file_handler(self, filename):
        """Get file handler."""
        filehandler = logging.FileHandler(filename, encoding="utf-8")
        filehandler.setFormatter(self.formatter)
        return filehandler

    # 输出到控制台handler的函数定义
    def get_console_handler(self):
        """Get console handler."""
        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setFormatter(self.formatter)
        return console_handler


#console_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
#file_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

#console_handler.setFormatter(console_format)
#file_handler.setFormatter(file_format)

 
if __name__ == '__main__':
	my_logg = MyLog('my_lpgger').logger
	my_logg.info(f"代码开始运行的时间{datetime.now()}")
	my_logg.debug('看看debug')
	my_logg.error('This is a error')	
