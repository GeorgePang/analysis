#!./venv/bin/python 
"""API to.

Filename: Indecies.py
Author: George Pang
Contact: panglilimcse@139.com
"""
import os
#from futu import TrdEnv
#from futu import TrdMarket
#from futu import KLType
#from futu import OpenQuoteContext
#from futu import SubType

class KDJ:
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    def __init__(self, tempus=9, k_smoother=3, d_smoother=3):
        """Inite the class.
    
        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            self: self.
            tempus: temple instaces.
            k_smoother: k_smoother instaces.
            d_smoother: d_smoother instaces.

        Returns:
            A class instance.

        Raises:
            None
        """
        self.tempus = tempus
        self.k_smoother = k_smoother 
        self.d_smoother = d_smoother

    def __del__(self):
        """Del."""
        pass

    def is_valid_data(self, data):
        """Check if data is valid."""
        return len(data) > self.tempus

    def calculate_kdj(self, df):
        """Calculate the kdj."""
#        print('calculate kdj'.center(80, '-'))
        if not self.is_valid_data:
            return 'invalid data'

        # lowest in self.tempus
        lowest = df['low'].rolling(self.tempus).min()
        lowest.fillna(value=df['low'].expanding().min(), inplace=True)
    
        # highest in self.tempus
        highest = df['high'].rolling(self.tempus).max()
        highest.fillna(value=df['high'].expanding().max(), inplace=True)

        # calculate res
        rsv = (df.close - lowest) / (highest - lowest) * 100
                                                    
        # first self.tempus ones are nan, fill with 100
        rsv.fillna(value=100.0, inplace=True)

        # calculate k, d, j
        '''
        adjust should be Fasle
        ewm(), 指数加权滑动
        '''
        df['k'] = rsv.ewm(com=2, adjust=False).mean()
        df['d'] = df['k'].ewm(com=2, adjust=False).mean()
        df['j'] = 3 * df['k'] - 2 * df['d']

        return df


    def bull_or_bear(self, df):
        """Check is bull or bear."""
        k, d, j = df['k'].values, df['d'].values, df['j'].values
        result = 0
        # k > d
        if k[-1] > d[-1]:
            result += 1
            # d < 20 within 3 cycles
            if d[-3:].min() < 20 and j <= 0:
                result += 4
            counter = 0
            # k upcross d within 3 cycles 
            while counter > -3:
                counter -= 1
                if k[counter] <= d[counter]:
                    result += 2
                    break
            
        # k < d
        elif k[-1] < d[-1]:
            result -= 1
            # d > 80 within 3 cycles
            if d[-3:].max() > 80 and j[-3: -10] > 00:
                result -= 4
            counter = 0
            # k downcross d within 3 cycles
            while counter > -3:
                counter -= 1
                if k[counter] > d[counter]:
                    result -= 2
                    break

        return result


class MACD:
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    def __init__(self, long=21, short=8, mid=9):
        """Inite the class.
    
        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            self: self.
            long: long period instances.
            short: short period instance.
            mid: mid period instances.

        Returns:
            A class instance.

        Raises:
            None
        """
        self.long = long
        self.short = short
        self.mid = mid

    def __del__(self):
        """Del."""
        pass

    def get_macd(self, df):
        """Calculate macd."""
        ema_long = df['close'].rolling(self.long).mean()
        ema_short = df['close'].rolling(self.short).mean()

        df['dif'] = ema_short - ema_long
        df['dea'] =df['dif'].rolling(self.mid).mean()
        df['macd'] = 2* (df['dif'] - df['dea'])

        return df


    def bull_or_bear(self, df):
        """Check is bull or bear.""" 
        dif, dea, macd  = df['dif'].values, df['dea'].values, df['macd'].values
        result = 0
        # macd bull or bear 
        if macd[-1] > 0 and dif[-1] >= dea[-1]:
            result += 1
        else:
            result -= 1
        # dea bull or bear
        if dea[-1] > 0 and dif[-1] >= dea[-1]:
            result += 2
        else:
           result -= 2

        return result


# 主函数
if __name__ == '__main__':
    from dotenv import find_dotenv, load_dotenv
    load_dotenv(find_dotenv('../.env'))
    env_dist = os.environ
    
                                                                            
    
