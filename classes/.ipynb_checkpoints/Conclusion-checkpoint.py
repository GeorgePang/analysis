#!/usr/bin/env python
"""Conclusion from indeces and parttens.

Filename: HistoryKlines.py
Author: George Pang
Contact: panglilimcse@139.com
"""


class Conclusion:
    """The conclusion from indeces and parttens calculation.

    Tere is a combined idex of code, name, kind and timestamp.
    If oscllation, tendency and partten are all empty, no record would be avialable.


    Attributes:
        code: code for a specific stock, like HK.06806.
        name: name of this stock.
        timestamp: ISO format of datetime string when record is ava;iable.
        kind: the k line type(cycling phase).
        oscillation: result of kdj index.
        tendency: result of macd index.
        partten: special partten of klines.
        context: the fillay conclusion text at this time.
    """
    def __init__(
        self,
        code: str, # Column(String, primary_key=True, index=True, comment="股票代码")
        name: str, # Column(String, index=True,comment="股票名称")
        timestamp: str, # Column(String, comment="时间戳")
        kind: str, # Column(KLineType_enum, comment="周期")
        # oscillation: str, # Column(Oscillation_enum, comment='摆动指标')
        # tendency: str, # Column(Tendency_enum, comment="趋势指标")
        # partten: str, # Column(Pattern_enum, comment="K线模式")
        # context: str # Column(String, comment="结论内容")

    ):
        """Inite the class.

        Args:
            self: self.
            code: code for stock.
            name: name of this stock
            timestamp: the iso format datatime string.
            kind: K line type of cycling phase.

        Returns:
            A class instance.

        Raises:
            None
        """
        self.code = code
        self.name = name
        self.timestamp = timestamp
        self.kind = kind
        self.context = ''
        

    def oscillation(self, oscillation=None):
        """Get or set oscillation value.

        Args:
            self: self.
            oscillation: a string of oscillation value. The default value is None, 
                which means to get the self.oscillation value. 
                Otherwise, use the given string to set self.oscillation value.

        Returns:
            A string for self.oscillation value.

        Raises:
            None
        """
        if oscillation is None:
            return self.oscillation

        self.oscillation = oscillation
        return self.oscillation

        
    def tendency(self, tendency=None):
        """Get or set tendency value.

        Args:
            self: self.
            tendency: a string of tendency value. The default value is None, 
                which means to get the self.tendecy value. 
                Otherwise, use the given string to set self.tendency value.

        Returns:
            A string for self.tendency value.

        Raises:
            None
        """
        if tendency is None:
            return self.tendency

        self.tendency = tendency
        return self.tendency

        
    def partten(self, partten=None):
        """Get or set partten value.

        Args:
            self: self.
            partten: a string of partten value. The default value is None, 
                which means to get the self.parten value. 
                Otherwise, use the given string to set self.partten value.

        Returns:
            A string for self.partten value.

        Raises:
            None
        """
        if partten is None:
            return self.partten

        self.partten = partten
        return self.partten
        
  
