#!./venv/bin/python 
"""API to.

Filename: commons.py
Author: George Pang
Contact: panglilimcse@139.com
"""

# from futu import *
#from futu import OpenQuoteContext
#Quote_ctx = OpenQuoteContext(host='127.0.0.1', port=11111)  # 创建行情对象
# print(Quote_ctx)

#	NONE
#	未知市场
#	HK
#	香港市场
#	US
#	美国市场
#	SH
#	沪股市场
#	SZ
#	深股市场
#	SG
#	新加坡市场
#	JP
#	日本市场
#Markets = ['NONE', 'HK', 'US', 'SH', 'SZ', 'SG', 'JP']
Markets = ['HK', 'US', 'SH', 'SZ']


# ALL
# 所有板块
# INDUSTRY
# 行业板块
# REGION
# 地域板块 
# CONCEPT
# 概念板块
# OTHER
# 其他板块 
#Plates = ['ALL', 'INDUSTRY', 'REGION', 'CONCEPT', 'OTHER'] 
Plates = ['ALL', 'INDUSTRY', 'REGION', 'CONCEPT'] 


# SecurityType
# NONE
# 未知
# BOND
# 债券
# BWRT
# 一揽子权证
# STOCK
# 正股
# ETF
# 信托,基金
# WARRANT
# 窝轮
# IDX
# 指数
# PLATE
# 板块
# DRVT
# 期权
# PLATESET
# 板块集
# FUTURE
# 期货

SecurityType = [ 
    'NONE', 'BOND', 'BWRT', 'STOCK', 'ETF', 'WARRANT',
    'IDX', 'PLATE' , 'DRVT', 'PLATESET', 'FUTURE' 
] 
	
#K 线类型
#KLType

#NONE
#未知
#K_1M
#1分 K
#K_DAY
#日 K
#K_WEEK
#周 K
#K_MON
#月 K
#K_YEAR
#年 K
#K_5M
#5分 K
#K_15M
#15分 K
#K_30M
#30分 K
#K_60M
#60分 K
#K_3M
#3分 K
#K_QUARTER
#季 K
#KLineType = ['NONE', 'K_1M', 'K_DAY', 'K_WEEK', 'K_MON', 'K_YEAR', 
#'K_5M','K_15M', 'K_30M', 'K_60M', 'K_3M', 'K_QUARTER']
KLineType = ['K_YEAR', 'K_QUARTER', 'K_MON', 'K_WEEK', 'K_DAY', 'K_60M', 'K_15M']

