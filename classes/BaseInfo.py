#!./venv/bin/python 
"""API to.

Filename: BaseInfo.py
Author: George Pang
Contact: panglilimcse@139.com
"""
import os
import sys
import asyncio
from futu import RET_OK
if __name__ == '__main__':
    from commons import Quote_ctx
else:
    from .commons import Quote_ctx

sys.path.append(os.getcwd())

class BaseInfo:
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    def __init__(self, market, security_type,code):
        """Inite the class.
    
        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            self: self.
            market: market.
            security_type: security_type.
            code: code

        Returns:
            A class instance.

        Raises:
            None
        """
        self.market = market
        self.security_type = security_type
        self.code = code
        self.quote_ctx = Quote_ctx # 创建行情对象


    def __def__(self):
        """Del."""
        self.quote_ctx.close() # 结束后记得关闭当条连接，防止连接条数用尽


    async def get_base_info(self):
        """Fetch basic information."""
        ret, data = self.quote_ctx.get_stock_basicinfo(self.market,
                                                       self.security_type,
                                                       self.code)
        if ret == RET_OK:
    #        print(data)
            print(f"{data['code'][0]} :  {data['name'][0]}")   # 取第一条的名称
#            print(data['stock_name'].values.tolist()[:5])   # 转为 list
            return data.fillna('NONE')
        else:
            print('error:', data)
            return  data
        


if __name__ == '__main__':

    basic_info = BaseInfo('HK.06806')
    result = asyncio.run(basic_info.get_basic_info())
    print(type(result))
    print(result[:3])

    exit()

