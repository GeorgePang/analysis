#!./venv/bin/python 
"""API to.

Filename: Quoter.py
Author: George Pang
Contact: panglilimcse@139.com
"""
import os
import sys
from futu import MarketState
from futu import KLType
from futu import OpenQuoteContext
from futu import RET_OK
from datetime import datetime
from dotenv import find_dotenv, load_dotenv
if __name__ == '__main__' or __package__ == '':
    from MyLogger import MyLog
    from Indecies import KDJ, MACD
else:
    from .MyLogger import MyLog
    from .Indecies import KDJ, MACD

sys.path.append(os.getcwd())

print(f'package: {__package__}')
print(f'name: {__name__}')

logger = MyLog('Quoter.log').logger


load_dotenv(find_dotenv('../.env'))
env_dist = os.environ

FUTUOPEND_ADDRESS = os.getenv('FUTUOPEND_ADDRESS')
FUTUOPEND_PORT = int(os.getenv('FUTUOPEND_PORT'))
TRADING_PWD =  os.getenv('TRADING_PWD') # 交易密码，用于解锁交易

#TRADING_ENVIRONMENT = TrdEnv.SIMULATE  # 交易环境：真实 / 模拟
#TRADING_MARKET = TrdMarket.HK  # 交易市场权限，用于筛选对应交易市场权限的账户
#TRADING_PERIOD = KLType.K_30M  # 信号 K 线周期
                                                                        
class Quoter:
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    def __init__(self, code: str, trading_period: str):
        """Inite the class.
    
        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            self: self.
            code:  code of stock.
            trading_period: trading time gap.
            maxium_amount_per_trade: The largest amount per trade.
            position_static: the minium_position.
            bull_or_bear: boolean.

        Returns:
            A class instance.

        Raises:
            None
        """
        self.code = code
        if code[:2] == 'HK':
            pass
        if code[:2] == 'SH' or code[:2] == 'SZ':
            pass

        self.quote_context = OpenQuoteContext(
            host=FUTUOPEND_ADDRESS, 
            port=FUTUOPEND_PORT
        )  # 行情对象
        self.trading_period = trading_period

        self.quote_context.subscribe(code_list=[self.code],subtype_list=[self.trading_period])
                                                     
#        ret, data = self.quote_context.query_subscription()
#        if ret == RET_OK:
#                print(data)
#        else:
#                print('error:', data)


    def __del__(self):
        """Close Quoter instac to save connection resource."""
        # 结束后记得关闭当条连接，防止连接条数用尽
        self.quote_context.close() 

    
    # 获取市场状态
    def is_normal_trading_time(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        ret, data = self.quote_context.get_market_state([self.code])
        if ret != RET_OK:
            print('获取市场状态失败：', data)
            return False
        market_state = data['market_state'][0]
        '''
        MarketState.MORNING            港、A 股早盘
        MarketState.AFTERNOON          港、A 股下午盘，美股全天
        MarketState.FUTURE_DAY_OPEN    港、新、日期货日市开盘
        MarketState.FUTURE_OPEN        美期货开盘
        MarketState.NIGHT_OPEN         港、新、日期货夜市开盘
        '''
    
        if market_state == MarketState.MORNING or \
                        market_state == MarketState.AFTERNOON or \
                        market_state == MarketState.FUTURE_DAY_OPEN  or \
                        market_state == MarketState.FUTURE_OPEN  or \
                        market_state == MarketState.NIGHT_OPEN:
            return True
        print('现在不是持续交易时段。')
        self.__del__()

        return False
    

    # 计算下单数量
    def calculate_quantity(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        price_quantity = 0
        # 使用最小交易量 * multiplier 
        ret, data = self.quote_context.get_market_snapshot([self.code])
        if ret != RET_OK:
            print('获取快照失败：', data)
            return price_quantity

        ask, bid = self.get_ask_and_bid()

        price_quantity = self.maxium_amount_per_trade // \
                ( data['lot_size'][0] * bid[0][0] )
                                                         
        return price_quantity * data['lot_size'][0] 
    
    
    # 拉取 K 线，计算KDJ, MACD，判断多空
    def calculate_bull_bear(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
    #    print(f'calculate_bull_bear {self.trading_period}'.rjust(80, '-'))
        ret, data = self.quote_context.get_cur_kline(
            code=self.code, 
            num=100, 
            ktype=self.trading_period
        )
        if ret != RET_OK:
            logger.info('获取K线失败：', data)
            return 0


        kdj =KDJ(9,3,3)
        macd = MACD(21, 8,9)

        kdj_data = kdj.calculate_kdj(data)
        kdj_bull_or_bear = kdj.bull_or_bear(kdj_data )

        macd_data = macd.get_macd(data)
        macd_bull_or_bear = macd.bull_or_bear(macd_data)

        #logger.info(f'KDJ is {kdj_bull_or_bear}, MACD is {macd_bull_or_bear}')

        return (kdj_bull_or_bear, macd_bull_or_bear)

    
    ############################ 填充以下函数来完成您的策略 ############################
    # 每次产生一根新的 K 线运行一次，可将策略的主要逻辑写在此处
    def on_bar_open(self):
        """Run once a bar finished."""
        # 打印分隔线
        #logger.info(f'{self.code} : {datetime.now()} new bar created'.center(80, '='))
                        
        # 只在常规交易时段交易
        if not self.is_normal_trading_time():
            print('not in tading time'.rjust(80,'-'))
            return False
    
        # 获取 K 线，计算均线，判断多空
        kdj_bull_or_bear,macd_bull_or_bear = self.calculate_bull_bear()
        logger.info(f'{self.code}: 【时间】datetime:{datetime.now()}')
        logger.info(f'{self.code}:\
                    kdj_bull_or_bear:{kdj_bull_or_bear} \
                    macd_bull_or_bear:{macd_bull_or_bear}'
                   )
                                           
        #logger.info(f'{self.code}: 【最大仓位】{maxium_position}')
        #logger.info(f'{self.code}: 【最小仓位】{minium_position}')
                                                              

# 主函数
if __name__ == '__main__':

    code = 'HK.01919'
    print(type(KLType))
    print(KLType.get_all_keys())

    """
    param_list = [
        (KLType, kdj_bull_or_bear, macd_bull_or_bear)
    ]
    """
    params = []
    quoter =Quoter(code, KLType.K_YEAR) 
    result = quoter.calculate_bull_bear()
    params.append((quoter.trading_period, result[0], result[1]))

    quoter =Quoter(code, KLType.K_MON) 
    result = quoter.calculate_bull_bear()
    params.append((quoter.trading_period, result[0], result[1]))

    quoter =Quoter(code, KLType.K_WEEK) 
    result = quoter.calculate_bull_bear()
    params.append((quoter.trading_period, result[0], result[1]))

    quoter =Quoter(code, KLType.K_DAY) 
    result = quoter.calculate_bull_bear()
    params.append((quoter.trading_period, result[0], result[1]))

    quoter =Quoter(code, KLType.K_30M) 
    result = quoter.calculate_bull_bear()
    params.append((quoter.trading_period, result[0], result[1]))

    position_static = 0
    for param in params:
        print(param)
        print(param[2])

        if param[2] > 0:
            position_static += 1

    print(position_static)
    os._exit(0)
