#!./venv/bin/python 
"""API to.

Filename: PlateList.py
Author: George Pang
Contact: panglilimcse@139.com
"""
import os
import sys
import asyncio
from futu import RET_OK
if __name__ == '__main__':
    from commons import Quote_ctx, Markets, Plates
else:
    from .commons import Quote_ctx, Markets, Plates

sys.path.append(os.getcwd())

class PlateList:
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    def __init__(self, market: str, plate_class: str):
        """Inite the class.
    
        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            self: self.
            market: market.
            plate_class:  code of stock.

        Returns:
            A class instance.

        Raises:
            None
        """
        self.market = market 
        self.plate_class = plate_class
        self.quote_ctx = Quote_ctx # 创建行情对象


    def __del__(self):
        """Close Quoter instac to save connection resource."""
        self.quote_ctx.close() # 结束后记得关闭当条连接，防止连接条数用尽


    async def get_plate_list(self):
        """Get plate list."""
        ret, data = self.quote_ctx.get_plate_list(self.market, self.plate_class)
        if ret == RET_OK:
            return data
            print(data['plate_name'][0])    # 取第一条的板块名称
            print(data['plate_name'].values.tolist()[:5])   # 转为 list
        else:
            print('error:', data)
            return 'error'


if __name__ == '__main__':
    print(Markets)
    print(Plates)

    plate_list = PlateList(Markets[1], Plates[0])
    result = asyncio.run(plate_list.get_plate_list())
    print(result)
    print(type(result))

    exit(0)

