#!./venv/bin/python 
"""API to.

Filename: PlateStocks.py
Author: George Pang
Contact: panglilimcse@139.com
"""
import os
import sys
import asyncio
from futu import RET_OK
if __name__ == '__main__':
    from commons import Quote_ctx
else:
    from .commons import Quote_ctx

sys.path.append(os.getcwd())

class PlateStocks:
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    def __init__(self, plate_code):
        """Inite the class.
    
        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            self: self.
            plate_code:  code of stock.

        Returns:
            A class instance.

        Raises:
            None
        """
        self.code = plate_code
        self.quote_ctx = Quote_ctx # 创建行情对象


    def __del__(self):
        """Close Quoter instac to save connection resource."""
        self.quote_ctx.close() # 结束后记得关闭当条连接，防止连接条数用尽


    async def get_plate_stock(self):
        """Get stock in plate."""
        ret, data = self.quote_ctx.get_plate_stock(self.code)
        if ret == RET_OK:
    #        print(data)
            print(data['stock_name'][:1])    # 取第一条的名称
#            print(data['stock_name'].values.tolist()[:5])   # 转为 list
            return data.fillna('NONE')
        else:
            print('error:', data)
            return  data
        


if __name__ == '__main__':

    plate_stocks = PlateStocks('SH.BK0271')
    result = asyncio.run(plate_stocks.get_plate_stock())
    print(type(result))
    print(result)

    exit()

