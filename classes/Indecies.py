#!./venv/bin/python 
"""API to.

Filename: Indecies.py
Author: George Pang
Contact: panglilimcse@139.com
"""
import os
#from futu import TrdEnv
#from futu import TrdMarket
#from futu import KLType
#from futu import OpenQuoteContext
#from futu import SubType

class KDJ:
    """Summary of class here.

    KDJ随机指标
    KDJ指标是由Dr.George Lane所创造的，是欧美期货常用的一套技术分
    析工具。由于期货风险性波动较大，需要比较短期且敏感的指标
    工具，因此中短期股票的技术分析也颇为适用。随机指标综合了
    动量观念、强弱指标与移动平均线的优点。KD线的随机观念，远
    比移动平均线实用很多。移动平均线在习惯上其以收盘价来计算，
    因而无法表现出一段行情的真正波幅，换句话说当日或最近数日
    的最高价、最低价，无法表现在移动平均线。因而有些专家才慢
    慢开创出一些更进步的技术理论，将移动平均线的应用发挥得淋
    离尽致。KD线就是其中一个颇具代表性的杰作。 
    随机指标在图表上采用K％和D％两条线，在设计中综合了动量观念
    、强弱指标与移动平均线的优点，在计算过程中主要研究高低价
    位与收市价的关系，反映价格走势的强弱和超买超卖现象。它的
    主要理论依据是：当价格上涨时，收市价倾向于接近当日价格区
    间的上端；相反，在下降趋势中收市价趋向于接近当日价格区间
    的下端。在股市和期市中，因为市场趋势上升而未转向前，每日
    多数都会偏向于高价位收市，而下跌时收市价就常会偏于低位。
    随机指数在设计中充分考虑价格波动的随机振幅与中短期波动的
    测算，使其短期测市功能比移动平均线更加准确有效，在市场短
    期超买超卖的预测方面又比强弱指数敏感，因此，这一指标被投
    资者广泛采用。
    计算公式
    RSV＝(CLOSE-LLV(lOW,N1))/(HHV(HIGH,N1)-LLV（LOW,N1))*100
    K＝MA（RSV，N2）
    D＝MA（K，N3）
    J=3*K-2*D 
    其中N1＝9，N2＝3，N3＝3
    K与D值永远介于0与1之间。
    KD线中的RSV，随着9日中高低价、收盘价的变动而有所不同。如果行情是一个明显的涨势，会带动K线（快速平均值）与D线（慢速平均值）向上升。但如果涨势开始迟缓，便会慢慢反应到K值与D值，使K线跌破D线，此时中短期跌势确立。由于KD线本质上是一个随机波动的观念，对于掌握中短期的行情走势非常正确。
    缺省时，系统在副图上绘制三条线，分别为RSV值的三日平均线K，K值的三日平均线D，三倍K值减二倍D值所得的J线。
    应用法则
    1．D％值在70以上时，市场呈现超买现象。D％值在30以下时市场则呈现超卖现象。 
    2．当Ｋ％线发生倾斜度趋于平缓时，是为警告讯号。 
    3．当K％值大于D％值，显示目前是向上涨升的趋势，因此在图形上K％向上突破D％线时，即为买进的讯号。 
    4．当D％值大于Ｋ％值，显示目前的趋势是向下跌落，因此在图形上K％向下跌破D％线，即为卖出讯号。 
    5．K％与D％线在特性上与强弱指标一样，当Ｋ％值与D％值在70以上，已显示超买的现象，30以下出现超卖的现象。唯强弱指标不能明显的显示买卖时机，而KD线则可以达到此目的。 
    6．背离讯号产生时，亦为非常正确的买进、卖出时机。 
    7．发行量太小，交易量太小的股票不适用KD指标；加权指数以及热门大型股准确性极高。 
    8．KD值在50左右交叉且为盘局时，此指标不能当成明显的买卖讯号。
    9．J％>100时为超买，J％<10时为超卖，反应较K％及D％为快。KD指标是计算ｎ日内买卖超情况，再加上平均线观念而求出的可设定计算期（即ｎ值），P1、P2、P3值。一般P1，P2都设成3，若P3设成0，表示不画J线。

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    def __init__(self, tempus=9, k_smoother=3, d_smoother=3):
        """Inite the class.
    
        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            self: self.
            tempus: temple instaces.
            k_smoother: k_smoother instaces.
            d_smoother: d_smoother instaces.

        Returns:
            A class instance.

        Raises:
            None
        """
        self.tempus = tempus
        self.k_smoother = k_smoother 
        self.d_smoother = d_smoother

    def __del__(self):
        """Del."""
        pass

    def is_valid_data(self, data):
        """Check if data is valid."""
        return len(data) > self.tempus

    def calculate_kdj(self, df):
        """Calculate the kdj."""
#        print('calculate kdj'.center(80, '-'))
        if not self.is_valid_data:
            return 'invalid data'

        # lowest in self.tempus
        lowest = df['low'].rolling(self.tempus).min()
        lowest.fillna(value=df['low'].expanding().min(), inplace=True)
    
        # highest in self.tempus
        highest = df['high'].rolling(self.tempus).max()
        highest.fillna(value=df['high'].expanding().max(), inplace=True)

        # calculate res
        rsv = (df.close - lowest) / (highest - lowest) * 100
                                                    
        # first self.tempus ones are nan, fill with 100
        rsv.fillna(value=100.0, inplace=True)

        # calculate k, d, j
        '''
        adjust should be Fasle
        ewm(), 指数加权滑动
        '''
        df['k'] = rsv.ewm(com=2, adjust=False).mean()
        df['d'] = df['k'].ewm(com=2, adjust=False).mean()
        df['j'] = 3 * df['k'] - 2 * df['d']

        return df


    def bull_or_bear(self, df):
        """Check is bull or bear."""
        k, d, j = df['k'].values, df['d'].values, df['j'].values
        result = 0
        # k > d
        if k[-1] > d[-1]:
            result += 1
            # d < 20 within 3 cycles
            if d[-3:].min() < 20 and j <= 0:
                result += 4
            counter = 0
            # k upcross d within 3 cycles 
            while counter > -3:
                counter -= 1
                if k[counter] <= d[counter]:
                    result += 2
                    break
            
        # k < d
        elif k[-1] < d[-1]:
            result -= 1
            # d > 80 within 3 cycles
            if d[-3:].max() > 80 and j[-3: -10] > 00:
                result -= 4
            counter = 0
            # k downcross d within 3 cycles
            while counter > -3:
                counter -= 1
                if k[counter] > d[counter]:
                    result -= 2
                    break

        return result


class MACD:
    """Summary of class here.

    MACD指数平滑移动平均线：
    原名是Moving Average Convergence and Divergence。本指标是
    一个很流行又管用的指标，可以去除掉移动平均线频烦的假讯号缺
    陷又能确保移动平均线最大的战果。

    计算公式：
    MACD是利用两条不同速度（长期与中期）的平滑移动平均线（EMA）
    来计算二者之间的差离状况作为研判行情的基础。在绘制的图形上，
    DIF与DEA形成了两条快慢移动平均线，买进卖出信号也就决定于这
    两条线的交叉点。很明显，MACD是一个中长期趋势的投资技术工具。

    DIF＝EMA（CLOSE，SHORT）－EMA（CLOSE，LONG）
    DEA＝EMA（DIF，MID）
    MACD＝（DIF－DEA）*2
    其中LONG=26，SHORT=12，MID=9

    缺省时，系统在副图上绘制SHORT=12，LONG=26，MID=9时的DIF线、DEA线、MACD线（柱状线）。
    用法：
    1.DIFF、DEA均为正，DIFF向上突破DEA，买入信号。
    2.DIFF、DEA均为负，DIFF向下跌破DEA，卖出信号。
    3.DEA线与K线发生背离，行情反转信号。
    4.分析MACD柱状线，由红变绿(正变负)，卖出信号；由绿变红，买入信号。
    5．MACD在0界限以上为多头市场，反之为空头市场。
    6．日线、周线、月线、分钟线配合运用效果会更好。


    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    def __init__(self, long=21, short=8, mid=9):
        """Inite the class.
    
        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            self: self.
            long: long period instances.
            short: short period instance.
            mid: mid period instances.

        Returns:
            A class instance.

        Raises:
            None
        """
        self.long = long
        self.short = short
        self.mid = mid

    def __del__(self):
        """Del."""
        pass

    def get_macd(self, df):
        """Calculate macd."""
        ema_long = df['close'].rolling(self.long).mean()
        ema_short = df['close'].rolling(self.short).mean()

        df['dif'] = ema_short - ema_long
        df['dea'] =df['dif'].rolling(self.mid).mean()
        df['macd'] = 2* (df['dif'] - df['dea'])

        return df


    def bull_or_bear(self, df):
        """Check is bull or bear.""" 
        dif, dea, macd  = df['dif'].values, df['dea'].values, df['macd'].values
        result = 0
        # macd bull or bear 
        if macd[-1] > 0 and dif[-1] >= dea[-1]:
            result += 1
        else:
            result -= 1
        # dea bull or bear
        if dea[-1] > 0 and dif[-1] >= dea[-1]:
            result += 2
        else:
           result -= 2

        return result


# 主函数
if __name__ == '__main__':
    from dotenv import find_dotenv, load_dotenv
    load_dotenv(find_dotenv('../.env'))
    env_dist = os.environ
    
                                                                            
    
