#!./venv/bin/python 
"""API to.

Filename: HistoryKlines.py
Author: George Pang
Contact: panglilimcse@139.com
"""
import os
import sys
import asyncio
import time
from datetime import datetime


class HistoryKlines:
    """The data from FutuOpenD and methods ti handle it.

    HOCLV data from FutuOpenD API.
    Methods to fetch this data.


    Attributes:
        code: code for a specific stock, like HK.06806.
        date: latest date for this serial of data.
    """
    def __init__(
        self, 
        code: str, 
        date: str =
                 datetime.now().isoformat().split('T', 0)
    ):
        """Inite the class.

        Args:
            self: self.
            code: code for stock.
            date: lates data and time.

        Returns:
            A class instance.

        Raises:
            None
        """
        self.code = code
        self.date =date 


if __name__ == '__main__':
    klines = HistoryKlines('HK.06806')
    print(klines)
    exit(0)


