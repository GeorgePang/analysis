#!./venv/bin/python 
"""API to.

Filename: Trader.py
Author: George Pang
Contact: panglilimcse@139.com
"""

import os
import sys
import time
from futu import TrdEnv
from futu import OpenQuoteContext
from futu import OpenSecTradeContext
from futu import SecurityFirm
from futu import MarketState
from futu import TrdMarket
from futu import TrdSide
from futu import OrderType
from futu import RET_OK
from futu import SubType
from futu import TRADING_SECURITY
from datetime import datetime
from dotenv import find_dotenv
from dotenv import load_dotenv

if __name__ == '__main__' or __package__ == '':
    from MyLogger import MyLog
    from Indecies import KDJ, MACD
else:
    from .MyLogger import MyLog
    from .Indecies import KDJ, MACD
sys.path.append(os.getcwd())

logger = MyLog('Strategies').logger

load_dotenv(find_dotenv('../.env'))
env_dist = os.environ

FUTUOPEND_ADDRESS = os.getenv('FUTUOPEND_ADDRESS')
FUTUOPEND_PORT = int(os.getenv('FUTUOPEND_PORT'))
TRADING_PWD =  os.getenv('TRADING_PWD') # 交易密码，用于解锁交易

TRADING_ENVIRONMENT = TrdEnv.SIMULATE  # 交易环境：真实 / 模拟
#TRADING_MARKET = TrdMarket.HK  # 交易市场权限，用于筛选对应交易市场权限的账户
#TRADING_PERIOD = KLType.K_30M  # 信号 K 线周期
                                                                        
class Trader:
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    def __init__(
        self, 
        code: str, 
        trading_period: str,
        maxium_amount_per_trade: int=1, 
        position_static: int=0,
        bull_or_bear: int=0
    ):
        """Inite the class.
    
        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            self: self.
            code:  code of stock.
            trading_period: trading time gap.
            maxium_amount_per_trade: The largest amount per trade.
            position_static: the minium_position.
            bull_or_bear: boolean.

        Returns:
            A class instance.

        Raises:
            None
        """
        self.code = code
        if code[:2] == 'HK':
            trading_market = TrdMarket.HK
        if code[:2] == 'SH' or code[:2] == 'SZ':
            trading_market = TrdMarket.CN

        self.quote_context = OpenQuoteContext(
            host=FUTUOPEND_ADDRESS, 
            port=FUTUOPEND_PORT
        )  # 行情对象
        self.trade_context =\
        OpenSecTradeContext(
            filter_trdmarket=trading_market, 
            host=FUTUOPEND_ADDRESS, 
            port=FUTUOPEND_PORT, 
            security_firm=SecurityFirm.FUTUSECURITIES
        )  # 交易对象，根据交易品种修改交易对象类型
        self.trading_period = trading_period
        self.maxium_amount_per_trade = maxium_amount_per_trade * 10000
        self.position_static = position_static
        self.bull_or_bear = bull_or_bear 

    def __del__(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        # 结束后记得关闭当条连接，防止连接条数用尽
        self.trade_context.close()
        self.quote_context.close() 


    # 解锁交易
    def unlock_trade(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        if TRADING_ENVIRONMENT == TrdEnv.REAL:
            ret, data = self.trade_context.unlock_trade(TRADING_PWD)
            if ret != RET_OK:
                print('解锁交易失败：', data)
                return False
            print('解锁交易成功！')
        return True
    
    
    # 获取市场状态
    def is_normal_trading_time(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        ret, data = self.quote_context.get_market_state([self.code])
        if ret != RET_OK:
            print('获取市场状态失败：', data)
            return False
        market_state = data['market_state'][0]
        '''
        MarketState.MORNING            港、A 股早盘
        MarketState.AFTERNOON          港、A 股下午盘，美股全天
        MarketState.FUTURE_DAY_OPEN    港、新、日期货日市开盘
        MarketState.FUTURE_OPEN        美期货开盘
        MarketState.NIGHT_OPEN         港、新、日期货夜市开盘
        '''
    
        if market_state == MarketState.MORNING or \
                        market_state == MarketState.AFTERNOON or \
                        market_state == MarketState.FUTURE_DAY_OPEN  or \
                        market_state == MarketState.FUTURE_OPEN  or \
                        market_state == MarketState.NIGHT_OPEN:
            return True
        print('现在不是持续交易时段。')
        self.__del__()

        return False
    
    
    # 获取持仓数量
    def get_holding_position(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        holding_position = 0
        ret, data = self.trade_context.position_list_query(
            code=self.code, 
            trd_env=TRADING_ENVIRONMENT
        )
        if ret != RET_OK:
            logger.info('获取持仓数据失败：', data)
            return None
        else:
            if data.shape[0] > 0:
                holding_position = data['qty'][0]
        return holding_position
    
    # get sleep time from TERADING_PERIOD
    def get_sleeping_time(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        return int(self.trading_period[2:-1]) * 60 

    # get klines
    def get_klines(self, length, kltype):
        """Fetches rows from FutuOpenD.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            self: data.
            length: How many lines to fetch.
            kltype: k line type.

        Returns:
            A futu data.

        Raises:
            None
        """
        ret, data = self.quote_context.get_cur_kline(self.code,  length, kltype)
        if ret != RET_OK:
            logger.info('获取K线失败：', data)
            return 0
        return data


    # 拉取 K 线，计算KDJ, MACD，判断多空
    def calculate_bull_bear(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        print(f'calculate_bull_bear {self.trading_period}'.rjust(80, '-'))
        ret, data = self.quote_context.get_cur_kline(
            code=self.code, 
            num=100, 
            ktype=self.trading_period
        )
        if ret != RET_OK:
            logger.info('获取K线失败：', data)
            return 0


        kdj =KDJ(9,3,3)
        macd = MACD(21, 8,9)

        kdj_data = kdj.calculate_kdj(data)
        kdj_bull_or_bear = kdj.bull_or_bear(kdj_data )

        macd_data = macd.get_macd(data)
        macd_bull_or_bear = macd.bull_or_bear(macd_data)

        #logger.info(f'KDJ is {kdj_bull_or_bear}, MACD is {macd_bull_or_bear}')

        return (kdj_bull_or_bear,  macd_bull_or_bear)

    
    # 获取一档摆盘的 ask1 和 bid1
    def get_ask_and_bid(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        ret, data = self.quote_context.get_order_book(self.code, num=5)
        if ret != RET_OK:
            print('获取摆盘数据失败：', data)
            return None, None
        return (data['Ask'], data['Bid'])
    
    
    # 开仓函数
    def open_position(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        # logger.info('opening code: {} '.format(code).center(60, '='))
        # 获取摆盘数据
        ask, bid = self.get_ask_and_bid()
    
        # 计算下单量
        open_quantity = self.calculate_quantity()
        logger.info(f'ask:{ask} bid:{bid} open quantity:{open_quantity}'.rjust(60, '-'))
    
        # 判断购买力是否足够
        if self.is_valid_quantity(self.code, open_quantity, bid[0][0]):
            # logger.info('self.is_valid_quantity: {}\
            # '.format(self.is_valid_quantity(self.code, open_quantity, ask)))

            # 下单
            ret, data = self.trade_context.place_order(
                price=ask[0][0],
                qty=open_quantity,
                code=self.code,
                trd_side=TrdSide.BUY,
                order_type=OrderType.NORMAL, 
                trd_env=TRADING_ENVIRONMENT,
                remark='moving_average_strategy'
            )
            if ret != RET_OK:
                print('开仓失败：', data)
        else:
            mesg = self.is_valid_quantity(
                self.code, 
                open_quantity, 
                ask
            )
            logger.info(f'self.is_valid_quantity: {mesg}')  
            print('下单数量超出最大可买数量。')
    
    
    # 平仓函数
    def close_position(self, quantity):
        """Close postion.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            quantity: data.

        Returns:
            None

        Raises:
            None
        """
        logger.info(f'closing code: {self.code} '.center(60, '='))
        # 获取摆盘数据
        ask, bid = self.get_ask_and_bid()
        logger.info(f'ask: {ask};  bid:{ask} ')
    
        # 检查平仓数量
        if quantity == 0:
            print('无效的下单数量。')
            return False
    
        # 平仓
        ret, data = self.trade_context.place_order(
            price=bid[0][0],
            qty=quantity, 
            code=self.code,
            trd_side=TrdSide.SELL,
            order_type=OrderType.NORMAL, 
            trd_env=TRADING_ENVIRONMENT, 
            remark='moving_average_strategy'
        )
        print(ret, data)

        if ret != RET_OK:
            print('平仓失败：', data)
            return False
        return True
    
    
    # 计算下单数量
    def calculate_quantity(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        price_quantity = 0
        # 使用最小交易量 * multiplier 
        ret, data = self.quote_context.get_market_snapshot([self.code])
        if ret != RET_OK:
            print('获取快照失败：', data)
            return price_quantity

        ask, bid = self.get_ask_and_bid()

        price_quantity = self.maxium_amount_per_trade //\
            ( data['lot_size'][0] 
            * bid[0][0] 
           )
                                                         
        return price_quantity * data['lot_size'][0] 
    
    
    # 判断购买力是否足够
    def is_valid_quantity(self, code, quantity, price):
        """Check if money is enough to buy.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            code: data.
            quantity: quantity of this bill.
            price: the price.

        Returns:
            boolean.

        Raises:
            None
        """
        ret, data = self.trade_context.acctradinginfo_query(
            order_type=OrderType.NORMAL, 
            code=code, 
            price=price,
            trd_env=TRADING_ENVIRONMENT
        )
        if ret != RET_OK:
            print('获取最大可买可卖失败：', data)
            return False
        max_can_buy = data['max_cash_buy'][0]
        max_can_sell = data['max_sell_short'][0]
        if quantity > 0:
            return quantity < max_can_buy
        elif quantity < 0:
            return abs(quantity) < max_can_sell
        else:
            return False
    
    
    # 展示订单回调
    def show_order_status(self, data):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        order_status = data['order_status'][0]
        order_info = dict()
        order_info['代码'] = data['code'][0]
        order_info['价格'] = data['price'][0]
        order_info['方向'] = data['trd_side'][0]
        order_info['数量'] = data['qty'][0]
        print('【订单状态】', order_status, order_info)


    ############################ 填充以下函数来完成您的策略 ############################
    # 策略启动时运行一次，用于初始化策略
    def on_init(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        # 解锁交易（如果是模拟交易则不需要解锁）
        if not self.unlock_trade():
            return False
        print(f'{self.code} {self.trading_period} 策略开始运行')
        return True
    
    
    # 每个 tick 运行一次，可将策略的主要逻辑写在此处
    def on_tick(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        pass
    
    
    # 每次产生一根新的 K 线运行一次，可将策略的主要逻辑写在此处
    def on_bar_open(self):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        # 打印分隔线
        logger.info(f'{self.code} : {datetime.now()} new bar created'.center(80, '='))
                        
        # 只在常规交易时段交易
        if not self.is_normal_trading_time():
            print('not in tading time'.rjust(80,'-'))
            return False
    
        # 获取 K 线，计算均线，判断多空
        if self.calculate_bull_bear == 0:
            kdj_bull_or_bear,macd_bull_or_bear = self.calculate_bull_bear()
            logger.info(f'{self.code}: 【时间】datetime:{datetime.now()}')
            logger.info(
                f'{self.code}: kdj_bull_or_bear:{kdj_bull_or_bear}\
                macd_bull_or_bear:{macd_bull_or_bear}')
            self.calculate_bull_bear = kdj_bull_or_bear

        # 获取持仓数量
        holding_position = self.get_holding_position()
        logger.info(f'{self.code} 【持仓状态】 的持仓数量为：{holding_position}')
        dealing_quantity = self.calculate_quantity() 
        logger.info(f'dealing_quantity: {dealing_quantity}')
        maxium_position = dealing_quantity * (self.position_static + 1) 
        minium_position = dealing_quantity * self.position_static 
        logger.info(f'{self.code}: 【最大仓位】{maxium_position}')
        logger.info(f'{self.code}: 【最小仓位】{minium_position}')
                                                              
        # 下单判断
        if self.bull_or_bear > 0:
            if holding_position <= maxium_position:
                logger.info(f'{self.code} 【操作信号】做多信号，建立多单。')
                self.open_position()
            else:
                logger.info(f'{self.code} 【操作信号】做多信号，无需加仓。')
        else:
            if holding_position > minium_position:
                logger.info(f'{self.code} 【操作信号】做空信号，平掉持仓。')
                self.close_position( holding_position-minium_position)
            else: 
                logger.info(f'{self.code} 【操作信号】做空信号，不开空单。')
    
        logger.info(f'{self.code} holding_position {holding_position}'.rjust(60, '-'))
        time.sleep(1)
    
    # 委托成交有变化时运行一次
    def on_fill(self, data):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        logger.inf('fill'.center(60, '-'))
    
    
    # 订单状态有变化时运行一次
    def on_order_status(self, data):
        """Fetches rows from a Smalltable.

        Retrieves rows pertaining to the given keys from the Table instance
        represented by table_handle.  String keys will be UTF-8 encoded.

        Args:
            data: data.
            keys: A sequence of strings representing the key of each table
              row to fetch.  String keys will be UTF-8 encoded.
            require_all_keys: If True only rows with values set for all keys will be
              returned.

        Returns:
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
             b'Zim': ('Irk', 'Invader'),
             b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).

        Raises:
            IOError: An error occurred accessing the smalltable.
        """
        logger.info('order status'.center(60, '-'))
        if data['code'][0] == TRADING_SECURITY:
            self.show_order_status(data)


# 主函数
if __name__ == '__main__':

    code = 'HK.01919'
    trader = Trader(code, 'K_30M', 10) 

    trader.get_sleeping_time()

    while not trader.is_normal_trading_time():
        logger.info('Not trading time now!')
        trader.__del__()
        #time.sleep(1800) 

    logger.info('---------- Trading time now!')

    trader.quote_context.subscribe(code_list=[code],
                                   subtype_list=[SubType.ORDER_BOOK,trader.trading_period])
                                                 
    ret, data = trader.quote_context.query_subscription()
    if ret == RET_OK:
            print(data)
    else:
            print('error:', data)

    logger.info('---------- Trading time now?')


    # 初始化策略
    if  trader.on_init():
        trader.on_bar_open()
    else:
        print('策略初始化失败，脚本退出！')
        trader.quote_context.close()
        trader.trade_context.close()

    os._exit(0)
