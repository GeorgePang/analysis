# from futu import *
from futu import OpenQuoteContext


Quote_ctx = OpenQuoteContext(host='127.0.0.1', port=11111)  # 创建行情对象
# print(Quote_ctx)

#	NONE
#	未知市场
#	HK
#	香港市场
#	US
#	美国市场
#	SH
#	沪股市场
#	SZ
#	深股市场
#	SG
#	新加坡市场
#	JP
#	日本市场
#Markets = ['NONE', 'HK', 'US', 'SH', 'SZ', 'SG', 'JP']
Markets = ['HK', 'US', 'SH', 'SZ']


# ALL
# 所有板块
# INDUSTRY
# 行业板块
# REGION
# 地域板块 
# CONCEPT
# 概念板块
# OTHER
# 其他板块 
#Plates = ['ALL', 'INDUSTRY', 'REGION', 'CONCEPT', 'OTHER'] 
Plates = ['ALL', 'INDUSTRY', 'REGION', 'CONCEPT'] 


# SecurityType
# NONE
# 未知
# BOND
# 债券
# BWRT
# 一揽子权证
# STOCK
# 正股
# ETF
# 信托,基金
# WARRANT
# 窝轮
# IDX
# 指数
# PLATE
# 板块
# DRVT
# 期权
# PLATESET
# 板块集
# FUTURE
# 期货

SecurityType = [ 'NONE', 'BOND', 'BWRT', 'STOCK', 'ETF', 'WARRANT', 'IDX', 'PLATE' , 'DRVT', 'PLATESET', 'FUTURE' ] 
	
#K 线类型
#KLType

#NONE
#未知
#K_1M
#1分 K
#K_DAY
#日 K
#K_WEEK
#周 K
#K_MON
#月 K
#K_YEAR
#年 K
#K_5M
#5分 K
#K_15M
#15分 K
#K_30M
#30分 K
#K_60M
#60分 K
#K_3M
#3分 K
#K_QUARTER
#季 K
#KLineType = ['NONE', 'K_1M', 'K_DAY', 'K_WEEK', 'K_MON', 'K_YEAR', 'K_5M','K_15M', 'K_30M', 'K_60M', 'K_3M', 'K_QUARTER']
KLineType = ['K_YEAR', 'K_QUARTER', 'K_MON', 'K_WEEK', 'K_DAY', 'K_30M', 'K_60M']


#财务过滤属性
#stockField
# 
# NONE
# 未知
# NET_PROFIT
# 净利润 
# NET_PROFIX_GROWTH
# 净利润增长率 
# SUM_OF_BUSINESS
# 营业收入 
# SUM_OF_BUSINESS_GROWTH
# 营收同比增长率 
# NET_PROFIT_RATE
# 净利率 
# GROSS_PROFIT_RATE
# 毛利率 
# DEBT_ASSETS_RATE
# 资产负债率 
# RETURN_ON_EQUITY_RATE
# 净资产收益率 
# ROIC
# 投入资本回报率 
# ROA_TTM
# 资产回报率 TTM 
# EBIT_TTM
# 息税前利润 TTM 
# EBITDA
# 税息折旧及摊销前利润 
# OPERATING_MARGIN_TTM
# 营业利润率 TTM 
# EBIT_MARGIN
# EBIT 利润率 
# EBITDA_MARGIN
# EBITDA 利润率 
# FINANCIAL_COST_RATE
# 财务成本率 
# OPERATING_PROFIT_TTM
# 营业利润 TTM 
# SHAREHOLDER_NET_PROFIT_TTM
# 归属于母公司的净利润 
# NET_PROFIT_CASH_COVER_TTM
# 盈利中的现金收入比例 
# CURRENT_RATIO
# 流动比率 
# QUICK_RATIO
# 速动比率 
# CURRENT_ASSET_RATIO
# 流动资产率 
# CURRENT_DEBT_RATIO
# 流动负债率 
# EQUITY_MULTIPLIER
# 权益乘数 
# PROPERTY_RATIO
# 产权比率 
# CASH_AND_CASH_EQUIVALENTS
# 现金和现金等价物 
# TOTAL_ASSET_TURNOVER
# 总资产周转率 
# FIXED_ASSET_TURNOVER
# 固定资产周转率 
# INVENTORY_TURNOVER
# 存货周转率 
# OPERATING_CASH_FLOW_TTM
# 经营活动现金流 TTM 
# ACCOUNTS_RECEIVABLE
# 应收账款净额 
# EBIT_GROWTH_RATE
# EBIT 同比增长率 
# OPERATING_PROFIT_GROWTH_RATE
# 营业利润同比增长率 
# TOTAL_ASSETS_GROWTH_RATE
# 总资产同比增长率 
# PROFIT_TO_SHAREHOLDERS_GROWTH_RATE
# 归母净利润同比增长率 
# PROFIT_BEFORE_TAX_GROWTH_RATE
# 总利润同比增长率 
# EPS_GROWTH_RATE
# EPS 同比增长率 
# ROE_GROWTH_RATE
# ROE 同比增长率 
# ROIC_GROWTH_RATE
# ROIC 同比增长率 
# NOCF_GROWTH_RATE
# 经营现金流同比增长率 
# NOCF_PER_SHARE_GROWTH_RATE
# 每股经营现金流同比增长率 
# OPERATING_REVENUE_CASH_COVER
# 经营现金收入比 
# OPERATING_PROFIT_TO_TOTAL_PROFIT
# 营业利润占比 
# BASIC_EPS
# 基本每股收益 
# DILUTED_EPS
# 稀释每股收益 
# NOCF_PER_SHARE
# 每股经营现金净流量 

#财务过滤属性周期
#FinancialQuarter
# 
# NONE
# 未知
# ANNUAL
# 年报
# FIRST_QUARTER
# 一季报
# INTERIM
# 中报
# THIRD_QUARTER
# 三季报
# MOST_RECENT_QUARTER
# 最近季报

#自定义技术指标属性
# StockField
# 
# NONE
# 未知
# PRICE
# 最新价格
# MA
# 简单均线
# MA5
# 5日简单均线（不建议使用）
# MA10
# 10日简单均线（不建议使用）
# MA20
# 20日简单均线（不建议使用）
# MA30
# 30日简单均线（不建议使用）
# MA60
# 60日简单均线（不建议使用）
# MA120
# 120日简单均线（不建议使用）
# MA250
# 250日简单均线（不建议使用）
# RSI
# RSI 
# EMA
# 指数移动均线
# EMA5
# 5日指数移动均线（不建议使用）
# EMA10
# 10日指数移动均线（不建议使用）
# EMA20
# 20日指数移动均线（不建议使用）
# EMA30
# 30日指数移动均线（不建议使用）
# EMA60
# 60日指数移动均线（不建议使用）
# EMA120
# 120日指数移动均线（不建议使用）
# EMA250
# 250日指数移动均线（不建议使用）
# KDJ_K
# KDJ 指标的 K 值 
# KDJ_D
# KDJ 指标的 D 值 
# KDJ_J
# KDJ 指标的 J 值 
# MACD_DIFF
# MACD 指标的 DIFF 值 
# MACD_DEA
# MACD 指标的 DEA 值 
# MACD
# MACD 
# BOLL_UPPER
# BOLL 指标的 UPPER 值 
# BOLL_MIDDLER
# BOLL 指标的 MIDDLER 值 
# BOLL_LOWER
# BOLL 指标的 LOWER 值 
# VALUE
# 自定义数值（stock_field1 不支持此字段）

#相对位置
#RelativePosition

# NONE
# 未知
# MORE
# 大于，stock_field1 位于stock_field2 的上方
# LESS
# 小于，stock_field1 位于stock_field2 的下方
# CROSS_UP
# 升穿，stock_field1 从下往上穿stock_field2
# CROSS_DOWN
# 跌穿，stock_field1 从上往下穿stock_field2

#形态技术指标属性
# PatternField

# NONE
# 未知
# MA_ALIGNMENT_LONG
# MA多头排列（连续两天MA5>MA10>MA20>MA30>MA60，且当日收盘价大于前一天收盘价）
# MA_ALIGNMENT_SHORT
# MA空头排列（连续两天MA5<MA10<MA20<MA30<MA60，且当日收盘价小于前一天收盘价）
# EMA_ALIGNMENT_LONG
# EMA多头排列（连续两天EMA5>EMA10>EMA20>EMA30>EMA60，且当日收盘价大于前一天收盘价）
# EMA_ALIGNMENT_SHORT
# EMA空头排列（连续两天EMA5<EMA10<EMA20<EMA30<EMA60，且当日收盘价小于前一天收盘价）
# RSI_GOLD_CROSS_LOW
# RSI低位金叉（50以下，短线RSI上穿长线RSI（前一日短线RSI小于长线RSI，当日短线RSI大于长线RSI））
# RSI_DEATH_CROSS_HIGH
# RSI高位死叉（50以上，短线RSI下穿长线RSI（前一日短线RSI大于长线RSI，当日短线RSI小于长线RSI））
# RSI_TOP_DIVERGENCE
# RSI顶背离（相邻的两个K线波峰，后面的波峰对应的CLOSE>前面的波峰对应的CLOSE，后面波峰的RSI12值<前面波峰的RSI12值）
# RSI_BOTTOM_DIVERGENCE
# RSI底背离（相邻的两个K线波谷，后面的波谷对应的CLOSE<前面的波谷对应的CLOSE，后面波谷的RSI12值>前面波谷的RSI12值）
# KDJ_GOLD_CROSS_LOW
# KDJ低位金叉（D值小于或等于30，且前一日K值小于D值，当日K值大于D值）
# KDJ_DEATH_CROSS_HIGH
# KDJ高位死叉（D值大于或等于70，且前一日K值大于D值，当日K值小于D值）
# KDJ_TOP_DIVERGENCE
# KDJ顶背离（相邻的两个K线波峰，后面的波峰对应的CLOSE>前面的波峰对应的CLOSE，后面波峰的J值<前面波峰的J值）
# KDJ_BOTTOM_DIVERGENCE
# KDJ底背离（相邻的两个K线波谷，后面的波谷对应的CLOSE<前面的波谷对应的CLOSE，后面波谷的J值>前面波谷的J值）
# MACD_GOLD_CROSS_LOW
# MACD低位金叉（DIFF上穿DEA（前一日DIFF小于DEA，当日DIFF大于DEA））
# MACD_DEATH_CROSS_HIGH
# MACD高位死叉（DIFF下穿DEA（前一日DIFF大于DEA，当日DIFF小于DEA））
# MACD_TOP_DIVERGENCE
# MACD顶背离（相邻的两个K线波峰，后面的波峰对应的CLOSE>前面的波峰对应的CLOSE，后面波峰的macd值<前面波峰的macd值）
# MACD_BOTTOM_DIVERGENCE
# MACD底背离（相邻的两个K线波谷，后面的波谷对应的CLOSE<前面的波谷对应的CLOSE，后面波谷的macd值>前面波谷的macd值）
# BOLL_BREAK_UPPER
# BOLL突破上轨（前一日股价低于上轨值，当日股价大于上轨值）
# BOLL_BREAK_LOWER
# BOLL突破下轨（前一日股价高于下轨值，当日股价小于下轨值）
# BOLL_CROSS_MIDDLE_UP
# BOLL向上破中轨（前一日股价低于中轨值，当日股价大于中轨值）
# BOLL_CROSS_MIDDLE_DOWN
# BOLL向下破中轨（前一日股价大于中轨值，当日股价小于中轨值）

#
