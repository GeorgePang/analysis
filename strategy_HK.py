#!./venv/bin/python 
"""trading stategies for HK shares.

Filename: strategies_HK.py
Author: George Pang
Contact: panglilimcse@139.com
"""

import os
import sys
import time
from futu import KLType, SubType
from classes.Trader import Trader
from classes.Quoter import Quoter
#from classes.MyLogger import MyLog
    
#log = MyLog('K_30M')   
#logger = log.logger    


codes = [
    ('HK.01919',10), ('HK.06806', 10), ('HK.00998', 10), \
    ('HK.00857',10), ('HK.01186', 10), ('HK.02318', 10),\
        ]
trader_test_hk = Trader('HK.800000', 'K_60M', 1)

if not trader_test_hk.is_normal_trading_time():
    trader_test_hk.__del__()
    sys.exit(0)

for stock in codes:
    code, amount = stock
    """
    param_list = [
        (KLType, kdj_bull_or_bear, macd_bull_or_bear)
    ]
    """
    params = []
    quoter =Quoter(code, KLType.K_QUARTER)
    result = quoter.calculate_bull_bear()
    params.append((quoter.trading_period, result[0], result[1]))

    quoter =Quoter(code, KLType.K_MON) 
    result = quoter.calculate_bull_bear()
    params.append((quoter.trading_period, result[0], result[1]))

    quoter =Quoter(code, KLType.K_WEEK) 
    result = quoter.calculate_bull_bear()
    params.append((quoter.trading_period, result[0], result[1]))

    quoter =Quoter(code, KLType.K_DAY) 
    result = quoter.calculate_bull_bear()
    params.append((quoter.trading_period, result[0], result[1]))

    position_static = 0
    for param in params:

        if param[2] > 0:
            position_static += 1

    quoter =Quoter(code, KLType.K_60M) 
    result = quoter.calculate_bull_bear()
    bull_or_bear =  result[0]

    trader = Trader(code, KLType.K_60M, amount, position_static, bull_or_bear) 
    if  not trader.is_normal_trading_time():
        trader.__del__()
        os._exit(0)
             
    trader.quote_context.subscribe(code_list=[code],subtype_list=[SubType.ORDER_BOOK,trader.trading_period])

    # 初始化策略
    if  trader.on_init():
        trader.on_bar_open()
    else:
        print(f'{code} 策略初始化失败，脚本退出！')
        trader.quote_context.close()
    time.sleep(9)

os._exit(0)
