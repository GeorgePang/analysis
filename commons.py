#!./venv/bin/python 
"""API to.

Filename: commons.py
Author: George Pang
Contact: panglilimcse@139.com
"""

from pandas import DataFrame


def transform_to_datarows(data: DataFrame):
    """Transform data from pandas DataFrame to dict.

    data from FutuOpend is pandas DataFrame, 
    it have to be transform to a list of dict to work with sqlalchemy.

    Args:
        data: DataFrame.

    Returns:
        A list of dicts mapping keys to the corresponding data.

    Raises:
        IOError: An error occurred accessing the smalltable.
    """
    # print('transform_to_datarows'.center(60, '-'))
    if type(data) is not DataFrame:
        return None

    columns = (data.columns.values.tolist())
    values = (data.values.tolist())
    data_row_length = len(columns)
    data_rows = []

    for item in values:
        data_row = {}
        i = 0

        while i < data_row_length:
            data_row.update({columns[i]: item[i]})
            i += 1

        data_rows.append(data_row)

    return data_rows
