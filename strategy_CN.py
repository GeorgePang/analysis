#!./venv/bin/python 
"""trading stategies for CN shares.

Filename: strategies_CN.py
Author: George Pang
Contact: panglilimcse@139.com
"""

import os
import time
from futu import KLType
from futu import SubType
from classes.Trader import Trader
from classes.Quoter import Quoter
#from classes.MyLogger import MyLog
    
#log = MyLog('K_60M_')   
#logger = log.logger    

codes = [
    ('SH.600583', 10), ('SH.601919', 10), ('SH.600031', 10), \
    ('SH.600036', 10),\
    ('SZ.002027', 10), ('SZ.002673', 10), ('SZ.002078', 10),\
        ]
trader_test_cn = Trader('SH.000001', 'K_30M', 1)

if not trader_test_cn.is_normal_trading_time():
    trader_test_cn.__del__()
    os._exit()

print('Tring'.rjust(60, '='))

for stock in codes:
    code, amount = stock
    """
    param_list = [
        (KLType, kdj_bull_or_bear, macd_bull_or_bear)
    ]
    """
    params = []
    quoter =Quoter(code, KLType.K_YEAR) 
    result = quoter.calculate_bull_bear()
    params.append((quoter.trading_period, result[0], result[1]))

    quoter =Quoter(code, KLType.K_MON) 
    result = quoter.calculate_bull_bear()
    params.append((quoter.trading_period, result[0], result[1]))

    quoter =Quoter(code, KLType.K_WEEK) 
    result = quoter.calculate_bull_bear()
    params.append((quoter.trading_period, result[0], result[1]))

    quoter =Quoter(code, KLType.K_DAY) 
    result = quoter.calculate_bull_bear()
    params.append((quoter.trading_period, result[0], result[1]))

    position_static = 0
    for param in params:

        if param[2] > 0:
            position_static += 1

    quoter =Quoter(code, KLType.K_60M) 
    result = quoter.calculate_bull_bear()
    bull_or_bear =  result[0]

    trader = Trader(code, KLType.K_60M, amount, position_static, bull_or_bear) 
    if  not trader.is_normal_trading_time():
        trader.__del__()
        os._exit(0)
             
    trader.quote_context.subscribe(code_list=[code],subtype_list=[SubType.ORDER_BOOK,trader.trading_period])

    # 初始化策略
    if  trader.on_init():
        trader.on_bar_open()
    else:
        print(f'{code} 策略初始化失败，脚本退出！')
        trader.quote_context.close()
    time.sleep(9)

os._exit(0)
