#!./venv/bin/python 
"""API to.

Filename: main.py
Author: George Pang
Contact: panglilimcse@139.com
"""
from sql_app.sql import get_codes_from_history_klines
from routes.sql_data import save_kdj_macd

codes = get_codes_from_history_klines('K_60M')
for code in codes:
    save_kdj_macd(code[0], 'K_60M')

exit('finish save multi conclusions'.rjust(80, '*')) 

