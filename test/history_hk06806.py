from futu import *
quote_ctx = OpenQuoteContext(host='127.0.0.1', port=11111)


class HistoryKline(code: str):
    def __init__(self, code: str):
        self.code = code 
        self.listing_date = get_listing_date(code)

    
    def get_listing_date():
        pass


    def get_history_klines():
        ret, data, page_req_key = quote_ctx.request_history_kline(self.cod,
                                                          start='2020-01-31',
                                                          end='2023-01-30',
                                                          max_count=5)  # 每页5个，请求第一页
        if ret == RET_OK:
            print(data['code'][0])    # 取第一条的股票代码
            print(data['close'].values.tolist())   # 第一页收盘价转为 list
        else:
            print('error:', data)
        while page_req_key != None:  # 请求后面的所有结果
            print('*************************************')
            ret, data, page_req_key = quote_ctx.request_history_kline('HK.06806', start='2015-09-11', end='2023-01-30', max_count=5, page_req_key=page_req_key) # 请求翻页后的数据
            if ret == RET_OK:
                print(data)
            else:
                print('error:', data)
        print('All pages are finished!')
        quote_ctx.close() # 结束后记得关闭当条连接，防止连接条数用尽


if __name__ == "__main__":
    klines = new HistoryKline(HK.06806)
    klines.get_listing_date()
    klines.get_history_klines()

