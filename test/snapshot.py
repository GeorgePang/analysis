from futu import *

quote_ctx = OpenQuoteContext(host='127.0.0.1', port=11111)  # 创建行情对象

snapshot = quote_ctx.get_market_snapshot('HK.06806') # 获取港股 HK.05806 的快照数据
#print(quote_ctx.get_market_snapshot('HK.06806'))  # 获取港股 HK.05806 的快照数据
print(type(snapshot))
print(len(snapshot))
print(type(snapshot[0]))
print(snapshot[0])
print(type(snapshot[1]))
print(snapshot[1])
print(snapshot[1]['listing_date'])
print(type(snapshot[1]['listing_date']))
print(snapshot[1]['listing_date'])
listing_date = snapshot[1]['listing_date']
print(type(listing_date))
print(snapshot[1]['listing_date'][0])

# print(snapshot[1].to_dict())

result = snapshot[1].to_dict()

for r in result:
    print(type(r))
    print(r)
#    if r == 'listing_date':
#        print(r, result[r])

for i in result.items():
    print(type(i))
    print(i)
    if i[0] == 'listing_date':
        print(i)

quote_ctx.close() # 关闭对象，防止连接条数用尽


#trd_ctx = OpenSecTradeContext(host='127.0.0.1', port=11111)  # 创建交易对象
#print(trd_ctx.place_order(price=500.0, qty=100, code="HK.00700", trd_side=TrdSide.BUY, trd_env=TrdEnv.SIMULATE))  # 模拟交易，下单（如果是真实环境交易，在此之前需要先解锁交易密码）
#trd_ctx.close()  # 关闭对象，防止连接条数用尽

