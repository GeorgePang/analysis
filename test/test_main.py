# import os, sys
# sys.path.append(os.getcwd())

from fastapi.testclient import TestClient
from ..main import app

client = TestClient(app)


def test_root():
    response = client.get("/?token=GeorgePang")
    print(response.json())
    print('======================================')
#    assert response.status_code == 200
#    assert response.json() == {"message": "Hello Bigger Applications!"}
    assert response.json() == {"message": "Hello Bigger Applications!"}


def test_admin():
    response = client.post(
        "/admin/?token=GeorgePang",
        headers={"x-token": "GeorgePang Header"}
    )
    print(response)
    assert response.json() == {"message": "Admin getting schwifty"}
