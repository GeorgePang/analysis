from commons import Quote_ctx

def get_listing_date(stock_code):
#    print(stock_code)

    quote_ctx = Quote_ctx  # 创建行情对象

    snapshot = quote_ctx.get_market_snapshot(stock_code) # 获取港股 HK.05806 的快照数据
    quote_ctx.close() # 关闭对象，防止连接条数用尽

#    print( type(snapshot))
#    print( len(snapshot))
#    print( snapshot)

    result = snapshot[1].to_dict()['listing_date']
    print(type(result))

    return(result[0])


#trd_ctx = OpenSecTradeContext(host='127.0.0.1', port=11111)  # 创建交易对象
#print(trd_ctx.place_order(price=500.0, qty=100, code="HK.00700", trd_side=TrdSide.BUY, trd_env=TrdEnv.SIMULATE))  # 模拟交易，下单（如果是真实环境交易，在此之前需要先解锁交易密码）
#trd_ctx.close()  # 关闭对象，防止连接条数用尽


if __name__ == '__main__':
    result = get_listing_date('HK.06806')
    print(result)
