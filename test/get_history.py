from commons import Quote_ctx, RET_OK
from datetime import date

def get_history(code,start,end,max_count=5):

    quote_ctx = Quote_ctx

    ret, data, page_req_key = quote_ctx.request_history_kline('HK.06806',
                                                          start='2020-01-31',
                                                          end='2023-01-30',
                                                          max_count=5)  # 每页5个，请求第一页
    if ret == RET_OK:
        print(data)
        print(data['code'][0])    # 取第一条的股票代码
        print(data['close'].values.tolist())   # 第一页收盘价转为 list
    else:
        print('error:', data)
    while page_req_key is not None:  # 请求后面的所有结果
        print('*************************************')
        ret, data, page_req_key = quote_ctx.request_history_kline('HK.06806', start='2015-09-11', end='2023-01-30', max_count=5, page_req_key=page_req_key) # 请求翻页后的数据
        if ret == RET_OK:
            print(data)
        else:
            print('error:', data)
    print('All pages are finished!')
    
    quote_ctx.close() # 结束后记得关闭当条连接，防止连接条数用尽

def get_listing_date(stock_code):
    
    quote_ctx = Quote_ctx
    snapshot = quote_ctx.get_market_snapshot(stock_code) # 获取港股 HK.05806 的快照数据
    return snapshot[1]['listing_date'][0]
    quote_ctx.close() # 结束后记得关闭当条连接，防止连接条数用尽

if __name__ == '__main__':
    print(RET_OK)
    start = get_listing_date('HK.06806')
    print(start)
    end = date.today()
    print(end)

    get_history('HK.06806', start, end, 5)

