#!./venv/bin/python 
"""API to.

Filename: main.py
Author: George Pang
Contact: panglilimcse@139.com
"""
import uvicorn
from pathlib import Path
from fastapi import Depends, FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.openapi.docs import get_redoc_html
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.openapi.docs import get_swagger_ui_oauth2_redirect_html
from psutil import users

from dependencies import dependencies
from internal import admin
from routes import items, sql_data
from sql_app.routes import conclusion, klines,sql, users

# import settings

BASE_DIR = Path(__file__).resolve().parent
MEDIA_PATH = BASE_DIR / 'media'
print(BASE_DIR)

# app = FastAPI(dependencies=[Depends(dependencies.get_query_token)])
app = FastAPI(swagger_url='api')

app.mount(
    '/static',
    StaticFiles(directory=BASE_DIR / 'static' / 'swagger_ui'),
    name='static'
)

origins = [
    "http://101.35.200.235:8000",
    "http://localhost",
    "http://localhost:8000",
    "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(
    conclusion.router,
    # prefix="/conclusion",
    tags=["sql"],
    # dependencies=[Depends(dependencies.get_token_header)],
    responses={419: {"description": "postgresql"}},
)

app.include_router(
    klines.router,
    prefix="/sql",
    tags=["sql"],
    dependencies=[Depends(dependencies.get_token_header)],
    responses={419: {"description": "postgresql"}},
)

app.include_router(sql_data.router)
# app.include_router(conclusion.router)
# app.include_router(users.router)
app.include_router(items.router)
app.include_router(
    admin.router,
    prefix="/admin",
    tags=["admin"],
    dependencies=[Depends(dependencies.get_token_header)],
    responses={418: {"description": "I'm a teapot"}},
)

app.include_router(
    sql.router,
    prefix="/sql",
    tags=["sql"],
    dependencies=[Depends(dependencies.get_token_header)],
    responses={419: {"description": "postgresql"}},
)


app.include_router(
    users.router,
    prefix="/users",
    tags=["sql"],
    dependencies=[Depends(dependencies.get_token_header)],
    responses={419: {"description": "postgresql"}},
)

@app.get("/docs", include_in_schema=False)
async def custom_swagger_ui_html():
    """Fetches interfaces for Swagger documents and tests."""
    return get_swagger_ui_html(
        # openapi_url=app.openapi_url,
        openapi_url=app.openapi_url,
        title=app.title + " - Swagger UI",
        oauth2_redirect_url=app.swagger_ui_oauth2_redirect_url,
        swagger_js_url="static/swagger-ui-bundle.js",
        swagger_css_url="static/swagger-ui.css",
    )


@app.get(app.swagger_ui_oauth2_redirect_url, include_in_schema=False)
async def swagger_ui_redirect():
    """HTML for Wwagger redirection.

    This is a direct calling of fastapi.docs.get_swagger_ui_oauth2_redirect_html().

    Returns:
        html

    Raises:
        None

    """
    return get_swagger_ui_oauth2_redirect_html()


@app.get("/redoc", include_in_schema=False)
async def redoc_html():
    """Fetches interfaces for redoc.

    Retrieves htno pbject for redoc.

    Returns:
        html

    Raises:
        None

    """
    return get_redoc_html(
        openapi_url=app.openapi_url,
        title=app.title + " - ReDoc",
        redoc_js_url="/static/redoc.standalone.js",
    )


@app.get("/")
async def root():
    """Fetches root test result.

    Retrieves information for test of APIs.

    Returns:
        A dict mapping keys to the corresponding neddage.

    Raises:
        None
    """
    return {"message": "Hello! This is Analysis."}


if __name__ == '__main__':
    uvicorn.run(
        'main:app',
        host='0.0.0.0',
        port=9000,
        reload=True,
        workers=1,
        #reload=False,
        #workers=4,
        root_path=''
    )
