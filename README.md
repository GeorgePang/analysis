# analysis of stcks
# virtual enviroment 3.12.x

Conventional Commits
Now that we've covered basic commit structure of a good commit message, I'd like to introduce Conventional Commits to help provide some detail on creating solid commit messages.

At D2iQ, we use Conventional Commit which is a great practice among engineering teams. Conventional Commit is a formatting convention that provides a set of rules to formulate a consistent commit message structure like so:

<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
The commit type can include the following:

feat – a new feature is introduced with the changes
fix – a bug fix has occurred
chore – changes that do not relate to a fix or feature and don't modify src or test files (for example updating dependencies)
refactor – refactored code that neither fixes a bug nor adds a feature
docs – updates to documentation such as a the README or other markdown files
style – changes that do not affect the meaning of the code, likely related to code formatting such as white-space, missing semi-colons, and so on.
test – including new or correcting previous tests
perf – performance improvements
ci – continuous integration related
build – changes that affect the build system or external dependencies
revert – reverts a previous commit
The commit type subject line should be all lowercase with a character limit to encourage succinct descriptions.

The optional commit body should be used to provide further detail that cannot fit within the character limitations of the subject line description.

It is also a good location to utilize BREAKING CHANGE: <description> to note the reason for a breaking change within the commit.

The footer is also optional. We use the footer to link the JIRA story that would be closed with these changes for example: Closes D2IQ-<JIRA #> .

Full Conventional Commit Example
fix: fix foo to enable bar

This fixes the broken behavior of the component by doing xyz. 

BREAKING CHANGE
Before this fix foo wasn't enabled at all, behavior changes from <old> to <new>

Closes D2IQ-12345
To ensure that these committing conventions remain consistent across developers, commit message linting can be configured before changes are able to be pushed up. Commitizen is a great tool to enforce standards, sync up semantic versioning, along with other helpful features.

To aid in adoption of these conventions, it's helpful to include guidelines for commits in a contributing or README markdown file within your projects.

Conventional Commit works particularly well with semantic versioning (learn more at SemVer.org) where commit types can update the appropriate version to release.  You can also read more about Conventional Commits here.

Commit Message Comparisons
Review the following messages and see how many of the suggested guidelines they check off in each category.

Good
feat: improve performance with lazy load implementation for images
chore: update npm dependency to latest version
Fix bug preventing users from submitting the subscribe form
Update incorrect client phone number within footer body per client request
Bad
fixed bug on landing page
Changed style
oops
I think I fixed it this time?
empty commit messages
