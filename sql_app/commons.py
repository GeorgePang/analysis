#!./venv/bin/python 
"""Common args, function, and classes.

Filename: ccomms.py
Author: George Pang
Contact: panglilimcse@139.com
"""

import os
import sys
from sqlalchemy import Enum

if __package__ != '':
    from classes.commons import KLineType
else:
    sys.path.append(os.getcwd())
    sys.path.append(os.path.dirname(os.getcwd()))
    from classes.commons import KLineType

sex = ('male', 'female')
sex_enum = Enum(*sex, name='sex_enum', create_type=False)

#周期类型
KLineType_enum = Enum(*KLineType, name="KLineType_enum", create_type=False)

#摆动指标类型
#Oscillation
Oscillations = [
        ("底部反转", "上穿", "顶部反转", "下穿", ""),
        ("UP_BOTTOM", "UP_CROSS", "DOWN_TOP", "DOWN_CROSS", "")
        ]
Oscillation_enum = Enum(*Oscillations[1], name="Oscillation_enum", create_type=False)


#趋势指标
Tendencies = [
        ("强多","强空","看多", "看空","转多","转空", ""),
        ("LONG_STRONG", "SHORT_STRONG", "LONG","SHORT","LONG_TURN","SHORT_TURN", "")
        ]
        
Tendency_enum = Enum(*Tendencies[1], name="Tendency_enum", create_type=False)


#K线模式
Pattern = [
        ("晨星","暮星","红三兵","黑三兵","尖头底","尖头顶","平底","平顶","双底","双顶",\
                "多头炮","空头炮","长下影","长上影","仙人指路+","仙人指路-", ""
         ),
        ("STAR_MORNING","STAR_EVENING","3_SOLDIERS_RED","3_SOLDIERS_BLACK","FLOOR_SHARP",\
                "CEIL_SHARP","FLOOR_PLAIN","CEIL_PLAIN","FLOOR_DOUBLE","CETL_DOUBLE","LONG_MORTAR",\
                "SHORT_MORTAR","SHADOW_BOTTOM","SHADOW_TOP","DIRECTION+","DIRECTION-",\
                ""
         )
        ]
Pattern_enum = Enum(*Pattern[1], name="Pattern_enmu", create_type=False)


#行情市场
#Market

Markets = [
    ("未知市场", "香港市场", "美国市场", "沪股市场", "深股市场", \
     "新加坡市场", "日本市场"),
    ("NONE", "HK", "US", "SH", "SZ", "SG", "JP")
]
market_type_enum = Enum(*Markets[1], name='market_type_enum',
                        create_type=False)
#板块集合类型
#Plate
Plates = [
    ("所有板块", "行业板块", "地域板块", "概念板块", "其他板块"),
    ("ALL", "INDUSTRY", "REGION", "CONCEPT", "OTHER")
]
plate_type_enum = Enum(*Plates[1], name='plate_type_enmu', create_type=False)

# 股票类型
SecuritType = [
    ('无效', '未知','债券','一揽子债券','正股','信托基金',
     '窝论','指数','板块','期权','板块集','期货'),
    ('N/A', 'NONE', 'BOND', 'BWRT', 'STOCK', 'ETF', 
     'WARRANT', 'IDX','PLATE', 'DRVT', 'PLATESET', 'FUTURE')
]
stock_type_enum = Enum(*SecuritType[1], name='stcok_type_enum', create_type=False)

# 窝轮子类型
WrtType = [
    ('无效', '未知','认购窝论','认沽窝论','牛正','熊正','界内证'),
    ('N/A', 'NONE', 'CALL', 'PUT', 'BULL', 'BEAR', 'INLINE')
]
stock_child_type_enum = Enum(
    *WrtType[1],  
    name='stock_child_type_enum', 
    create_type=False
)

# 期权类型
OptionType = [
    ('未知','所有','看涨期权','看跌期权'),
    ('NONE','ALL','CALL','PUT')
]
option_type_enum = Enum(*OptionType[1], name='option_type_enum', create_type=True)

#
ExchangeType = [
    ('未知','港交所·主板','港交所·创业板','港交所','纽交所','纳斯达克','OTC市场','美交所','美国','NYMEX','COMEX','CBOT','CME','CBOE','上交所','深交所','科创板','新交所','大阪交易所'),
    ('NONE','HK_MAINBOARD','HK_GEMBOARD','HK_HKEX','US_NYSE','US_NASDAQ','US_PINK','US_AMEX','US_OPTION','US_NYMEX','US_COMEX','US_CBOT','US_CME','US_CBOE','CN_SH','CN_SZ','CN_STIB','SG_SGX','JP_OSE')
]
exchange_type_enum = Enum(*ExchangeType[1], name='exchange_type_enum',
                          create_type=False)

