#!./venv/bin/python 
"""Crud operations.

Filename: crus.py
Author: George Pang
Contact: panglilimcse@139.com
"""

import datetime
from pandas import DataFrame as df
from sqlalchemy.orm import Session
from sqlalchemy.sql import text
from sqlalchemy.dialects.postgresql import insert
from fastapi import Depends

from sql_app.models import models
from sql_app.models import users
from sql_app.models import history_klines
from sql_app.schemas.schemas import HistoryKline, UserCreate, ItemCreate
from sql_app.database import Base, engine, SessionLocal, get_db


def get_table_class(table_name: str):
    """Get class by k line type.

    Args:
        table_name: name of the table to stock codes.

    Returns:
        A class of history k line type for stock share.

    Raises:
        None
    """
    result = Base
    result = history_klines.K_DAY if table_name == 'K_DAY' else result
    result = history_klines.K_1M if table_name == 'K_1M' else result
    result = history_klines.K_3M if table_name == 'K_3M' else result
    result = history_klines.K_5M if table_name == 'K_5M' else result
    result = history_klines.K_15M if table_name == 'K_15M' else result
    result = history_klines.K_30M if table_name == 'K_30M' else result
    result = history_klines.K_60M if table_name == 'K_60M' else result
    result = history_klines.K_WEEK if table_name == 'K_WEEK' else result
    result = history_klines.K_MON if table_name == 'K_MON' else result
    result = history_klines.K_QUARTER if table_name == 'K_QUART' else result
    result = history_klines.K_QUARTER if table_name == 'K_QUARTER' else result
    result = history_klines.K_YEAR if table_name == 'K_YEAR' else result

    return result


def get_data_db(db: SessionLocal, data: dict):
    """Read data from db.

    Args:
        db: db session
        data:  dict, like {"module_name": string, "filter": string}

    Returns:
        A query result.

    Raisee:
        None
    """
    result = db.query(data['model_name'])
    result = result.filter(data['model_name'].code == data['filter'])
    result = result.filter(data['model_name'].kind == data['filters'][0])
    return result


def save_data_db(data, db: SessionLocal, set_update=None):
    """Save data to db.

    Args:
        data: dict, 
            {
                'table': table istance, 
                'values': list of dicts, 
                'sets': columns for update.
            }
        set_update: dict, if not None ,using update method when conflict. 

    Returns:
        None

    Raises:
        None
    """
    insert_stmt = insert(data['table']).values(data['values'])
    if set_update is not None:
        do_update_stmt = insert_stmt.on_conflict_do_update(
            constraint=None,
            index_elements=data['index'],
            index_where=None,
            set_=get_set(set_update, insert_stmt)
        )
        stmt = do_update_stmt
    else:
        do_nothing_stmt = insert_stmt.on_conflict_do_nothing(
            constraint=None,
            index_elements=data['index'],
            index_where=None
        )
        stmt = do_nothing_stmt

    with database.engine.connect() as connection:
        connection.execute(stmt)
        connection.commit()
    return


def create_history_kline(
        db: Session,
        history_kline_data: HistoryKline
):
    """Insert history kline.

    Args:
        db: db session.
        history_kline_data : data of the history kliness.

    Returns:
        None

    Raises:
        None
    """
    insert_stmt = insert(models.HistoryKline).values(history_kline_data)

    do_nothing_stmt = insert_stmt.on_conflict_do_nothing(
        index_elements=[
            'code',
            'time_key'
        ]
    )

    with engine.connect() as connection:
        connection.execute(do_nothing_stmt)
        connection.commit()
    return


def get_history_kline(
        db: SessionLocal,
        code: str,
        kltype: str = 'K_DAY'
):
    """Read history klines.

    Args:
        db: db session.
        code: code of the stock.
        kltype: type

    Returne:
        A query result.

    """
    print(f'code is {code}, kltype is {kltype}'.rjust(80, '-'))
    print(f'in crud.py/get_history_klinem db is {db}'.rjust(80, '-'))
    db_table = get_table_class(kltype)
    print(f'db_table is {db_table}'.rjust(80, '-'))

    kline = db.query(db_table).filter(
        db_table.code == code
    ).order_by(
        db_table.time_key.desc()
    ).limit(100).all()

    return kline


def save_history_kline(data, update=True):
    """Save history k lines.

    Args:
        data: data  
        update: boolean, update data if it is true when confilict

    Returns:
        None

    aises:
        None
    """
    insert_stmt = insert(get_table_class(data['table'])).values(data['values'])
    if update:
        do_update_stmt = insert_stmt.on_conflict_do_update(
            index_elements=data['index'],
            set_=dict(
                k=insert_stmt.excluded.k,
                d=insert_stmt.excluded.d,
                j=insert_stmt.excluded.j,
                dif=insert_stmt.excluded.dif,
                dea=insert_stmt.excluded.dea,
                macd=insert_stmt.excluded.macd
            )
        )
        with engine.connect() as connection:
            connection.execute(do_update_stmt)
            connection.commit()
        return
    else:
        do_nothing_stmt = insert_stmt.on_conflict_do_nothing(
            index_elements=['code', 'time_key']
        )

        with engine.connect() as connection:
            connection.execute(do_nothing_stmt)
            connection.commit()
        return


def get_code_list(db: Session, db_table=history_klines.K_DAY):
    """Read all stock codes.

    Args:
        db: db session
        db_table: table class of the stock codes by type

    Returns:
        A list of stock share code,which in our watching list ect.

    Raises:
        None
    """
    codes = db.query(db_table.code).distinct().all()
    return list(map(lambda code: (code[0], code[1]), codes))


def get_userid(db: Session, user_id: str):
    """Read user.

    Args:
        db: db session.
        user_id: id of the user.

    Returns:
        A list of user information
    """
    result = db.query(users.User).filter(users.User.id == user_id).first()
    return result


def get_user(db: Session, user_name: str):
    """Read user.

    Args:
        db: db session.
        user_name: name of the user.

    Returns:
        A list of user information
    """
    return db.query(users.User).filter(users.User.name == user_name).first()


def get_user_by_email(db: Session, email: str):
    """Read use by email.

    Args:
        db: db session
        email: user email.

    Returns:
        A list of user information.

    Raises:
        None
    """
    return db.query(users.User).filter(users.User.email == email).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    """Read user list.

    Args:
        db: db session
        skip: skiled recourds.
        limit: maxium read records.

    Returns:
        A list of user information.

    Raises:
        None
    """
    return db.query(users.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: UserCreate):
    """Create a new user.

    Args:
        db: db session
        user: user create schema

    Returns:
        A object of created user.

    Raises:
        None
    """
    fake_hashed_password = user.password + "notreallyhashed"
    db_user = users.User(email=user.email,
                         password=fake_hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_customers(db: Session, skip: int = 0, limit: int = 100):
    """Read user list.

    Args:
        db: db session
        skip: skiled recourds.
        limit: maxium read records.

    Returns:
        A list of user information.

    Raises:
        None
    """
    return db.query(users.Customer).offset(skip).limit(limit).all()


def get_invoices(db: Session, skip: int = 0, limit: int = 100):
    """Read user list.

    Args:
        db: db session
        skip: skiled recourds.
        limit: maxium read records.

    Returns:
        A list of user information.

    Raises:
        None
    """
    return db.query(users.Invoice).offset(skip).limit(limit).all()


def get_filtered_invoices(keyword: str, db: SessionLocal, skip: int = 0, limit: int = 6):
    """Get filtered invoices."""
    stmt = '''SELECT
        invoices.id,
        invoices.amount_in_cents as amount,
        invoices.date,
        invoices.status,
        customers.name,
        customers.email,
        customers.image_url
      FROM invoices
      JOIN customers ON invoices.customer_id = customers.id
      WHERE
        customers.name ILIKE :query OR
        customers.email ILIKE :query OR
        invoices.amount_in_cents::text ILIKE :query OR
        invoices.date::text ILIKE :query OR 
        invoices.status::text ILIKE :query
      ORDER BY invoices.date DESC
      LIMIT :ITEMS_PER_PAGE OFFSET :offset
    '''

    result = db.execute(
        text(stmt),
        {'query': '%' + keyword + '%',
         'ITEMS_PER_PAGE': limit,
         'offset': skip}
    )

    return result


def get_conclusion_pages(
        codes: tuple,
        db: SessionLocal,
        keyword: str,
        whole: bool = False
):
    """Get conclusion pages."""
    dates = '' if whole else datetime.date.today().isoformat()[:-3]

    stmt = '''SELECT COUNT(*)
    FROM conclusion
    WHERE
      conclusion.code IN  :codes AND
      conclusion.time_key::text ILIKE :dates AND 
      (
      conclusion.code ILIKE :query OR
      conclusion.name ILIKE :query OR
      conclusion.time_key ILIKE :query OR
      conclusion.kind::text ILIKE :query OR
      conclusion.oscillation::text ILIKE :query OR
      conclusion.tendency::text ILIKE :query OR
      conclusion.pattern::text ILIKE :query OR
      conclusion.context::text ILIKE :query
      )
    '''

    result = db.execute(
        text(stmt),
        {
            'codes': codes,
            'query': '%' + keyword + '%',
            'dates': dates + '%'
        }
    )

    return result


def get_invoice_pages(keyword: str, db: SessionLocal):
    """Get invoie pages."""
    # if keyword.lower() == 'paid' or keyword.lower() == 'pending':
    stmt = '''SELECT COUNT(*)
    FROM invoices
    JOIN customers ON invoices.customer_id = customers.id
    WHERE
      customers.name ILIKE :query OR
      customers.email ILIKE :query OR
      invoices.amount_in_cents::text ILIKE :query OR
      invoices.date::text ILIKE :query OR
      invoices.status::text ILIKE :query
    '''

    result = db.execute(text(stmt), {'query': '%' + keyword + '%'})

    return result


def delete_invoice(id: str, db: SessionLocal):
    """Delete invoice by id."""

    result = db.execute(text('DELETE FROM invoices WHERE id=:id'), {'id': id})
    db.commit()
    db.close()

    return result.rowcount


def get_revenue(db: SessionLocal, skip: int = 0, limit: int = 100):
    """Read revenue.

    Args:
        db: db session
        skip: skiled recourds.
        limit: maxium read records.

    Returns:
        A list of revenue information.

    Raises:
        None
    """
    return db.query(users.Revenue).offset(skip).limit(limit).all()


def get_latest_invoices(
        db: SessionLocal,
        skip: int = 0,
        limit: int = 5
):
    """Get latest incoices."""
    result = db.execute(
        text('SELECT invoices.amount_in_cents, customers.name, \
                    customers.image_url, customers.email \
                    FROM invoices \
                    JOIN customers\
                    ON invoices.customer_id=customers.id \
                    ORDER BY invoices.date DESC LIMIT 5'
             )
    )

    return result


def get_counts(tablename: str, db: SessionLocal):
    """Get counts."""
    stmt = f'SELECT COUNT(*) FROM {tablename}'

    return db.execute(text(stmt))


def get_invoice_status(db: SessionLocal):
    """Get invoice status."""
    stmt = "SELECT \
         SUM(CASE WHEN status = 'paid' \
            THEN amount_in_cents ELSE 0 END) AS \"paid\", \
         SUM(CASE WHEN status = 'pending' \
            THEN amount_in_cents ELSE 0 END) AS \"pending\" \
         FROM invoices"

    return db.execute(text(stmt))


def get_codes(tablename: str, db: SessionLocal, ):
    """Function summary.

    Args:
        db: db session
        tablename: name of the table to stock codes

    Returns:
        A list of stock share code,which in our watching list ect.

    Raises:
        None
    """
    print(f'in sql_app/cruds/crud.py/get_codes. db is {db}'.center(80, '-'))

    table = get_table_class(tablename)
    result = db.query(table.code, table.name).distinct().all()
    return list(map(lambda x: (x[0], x[1]), result))


async def get_basic_info(code: str, db: SessionLocal = Depends(get_db)):
    """Function summary.

    Args:
        db (object): db session
        code: stock code

    Returns:
        A query result of basic information for a stock share idendified by code.

    Raises:
        None
    """
    result = db.query(models.BaseInfo).filter(models.BaseInfo.code == code).first()
    return result


def get_last_conclusions(codes: list, limit: int, db: SessionLocal):
    """Get latest conclusions of codes."""
    print(f'crud to get last conclusions'.rjust(80, '-'))
    stmt = """SELECT * 
            FROM conclusion 
            WHERE code IN :codes
            ORDER BY time_key DESC 
            LIMIT :limit
            """

    result = db.execute(
        text(stmt),
        {
            'codes': codes,
            'limit': limit
        })

    return result


async def select_filtered_conclusions(
        codes: tuple,
        db: SessionLocal,
        keyword: str = '',
        skip: int = 0,
        limit: int = 6,
        whole: bool = False
):
    """Get filtered conclusions."""
    # today = datetime.date.today()
    # print(today.year, today.month, today.day)
    # dates = '' if whole else  datetime.date(today.year, today.month-3, today.day).isoformat()[:-3]

    # print(dates)

    stmt = '''SELECT
        conclusion.code,
        conclusion.name,
        conclusion.time_key,
        conclusion.kind,
        conclusion.oscillation,
        conclusion.tendency,
        conclusion.pattern,
        conclusion.context
      FROM conclusion
      WHERE
        conclusion.code IN :codes AND 
        (
            conclusion.code ILIKE :query OR
            conclusion.name ILIKE :query OR
            conclusion.time_key ILIKE :query OR 
            conclusion.kind::text ILIKE :query OR
            conclusion.oscillation::text ILIKE :query OR
            conclusion.tendency::text ILIKE :query OR
            conclusion.pattern::text ILIKE :query OR
            conclusion.context ILIKE :query
        )
      ORDER BY conclusion.time_key DESC
      LIMIT :ITEMS_PER_PAGE OFFSET :offset
    '''

    # conclusion.time_key::text ILIKE :dates AND

    result = db.execute(
        text(stmt),
        {'codes': codes,
         'query': '%' + keyword + '%',
         'ITEMS_PER_PAGE': limit,
         'offset': skip,
         # 'dates': dates+'%'
         }
    ).fetchall()

    return result
