#!./venv/bin/python 
"""Crud operations.

Filename: crus.py
Author: George Pang
Contact: panglilimcse@139.com
"""

import datetime

from fastapi import Depends
from pandas import DataFrame as df
from sqlalchemy.orm import Session
from sqlalchemy.sql import text
from sqlalchemy.dialects.postgresql import insert

from sql_app.models import models
from sql_app.models import users
from sql_app.models import history_klines
from sql_app.schemas.schemas import HistoryKline, UserCreate, ItemCreate
from sql_app.database import Base, engine, SessionLocal, get_db


async def read_conclusion_pages(
        codes: tuple,
        keyword: str,
        db: SessionLocal=Depends(get_db),
        whole: bool = False
):
    """Get conclusion pages."""
    dates = '' if whole else datetime.date.today().isoformat()[:-3]

    stmt = '''SELECT COUNT(*)
    FROM conclusion
    WHERE
      conclusion.code IN  :codes AND
      conclusion.time_key::text ILIKE :dates AND 
      (
      conclusion.code ILIKE :query OR
      conclusion.name ILIKE :query OR
      conclusion.time_key ILIKE :query OR
      conclusion.kind::text ILIKE :query OR
      conclusion.oscillation::text ILIKE :query OR
      conclusion.tendency::text ILIKE :query OR
      conclusion.pattern::text ILIKE :query OR
      conclusion.context::text ILIKE :query
      )
    '''

    result = db.execute(
        text(stmt),
        {
            'codes': codes,
            'query': '%' + keyword + '%',
            'dates': dates + '%'
        }
    ).fetchall()

    return result[0][0]


def get_last_conclusions(codes: list, limit: int, db: SessionLocal):
    """Get latest conclusions of codes."""
    print(f'crud to get last conclusions'.rjust(80, '-'))
    stmt = """SELECT * 
            FROM conclusion 
            WHERE code IN :codes
            ORDER BY time_key DESC 
            LIMIT :limit
            """

    result = db.execute(
        text(stmt),
        {
            'codes': codes,
            'limit': limit
        })

    return result


async def select_filtered_conclusions(
        codes: tuple,
        db: SessionLocal,
        keyword: str = '',
        skip: int = 0,
        limit: int = 6,
        whole: bool = False
):
    """Get filtered conclusions."""

    dates = '' if whole else datetime.date.today().isoformat()[:-3]

    print(dates)

    stmt = '''SELECT
        conclusion.code,
        conclusion.name,
        conclusion.time_key,
        conclusion.kind,
        conclusion.oscillation,
        conclusion.tendency,
        conclusion.pattern,
        conclusion.context
      FROM conclusion
      WHERE
        conclusion.code IN :codes AND 
        conclusion.time_key::text ILIKE :dates AND 
        (
            conclusion.code ILIKE :query OR
            conclusion.name ILIKE :query OR
            conclusion.time_key ILIKE :query OR 
            conclusion.kind::text ILIKE :query OR
            conclusion.oscillation::text ILIKE :query OR
            conclusion.tendency::text ILIKE :query OR
            conclusion.pattern::text ILIKE :query OR
            conclusion.context ILIKE :query
        )
      ORDER BY conclusion.time_key DESC
      LIMIT :ITEMS_PER_PAGE OFFSET :offset
    '''

    result = db.execute(
        text(stmt),
        {'codes': codes,
         'query': '%' + keyword + '%',
         'ITEMS_PER_PAGE': limit,
         'offset': skip,
         'dates': dates + '%'}
    )

    return result
