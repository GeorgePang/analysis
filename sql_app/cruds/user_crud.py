#!./venv/bin/python 
"""Crud operations.

Filename: crus.py
Author: George Pang
Contact: panglilimcse@139.com
"""


from sql_app.database import SessionLocal
from sql_app.models import users
from sql_app.schemas.schemas import UserCreate, ItemCreate


async def get_user_by_id(user_id: str, db: SessionLocal):
    """Read user by user id.

    Args:
        db: db session.
        user_id: id of the user.

    Returns:
        A list of user information
    """
    result = db.query(users.User).filter(users.User.id == user_id).first()
    return result


def get_user_by_name(db: SessionLocal, user_name: str):
    """Read user.

    Args:
        db: db session.
        user_name: name of the user.

    Returns:
        A list of user information
    """
    return db.query(users.User).filter(users.User.name == user_name).first()


def get_user_by_email( email: str, db: SessionLocal):
    """Read use by email.

    Args:
        db: db session
        email: user email.

    Returns:
        A list of user information.

    """
    return db.query(users.User).filter(users.User.email == email).first()


def get_users(db: SessionLocal, skip: int = 0, limit: int = 100):
    """Read user list.

    Args:
        db: db session
        skip: skiled recourds.
        limit: maxium read records.

    Returns:
        A list of user information.

    Raises:
        None
    """
    return db.query(users.User).offset(skip).limit(limit).all()


def create_user(db: SessionLocal, user: UserCreate):
    """Create a new user.

    Args:
        db: db session
        user: user create schema

    Returns:
        A object of created user.

    Raises:
        None
    """
    fake_hashed_password = user.password + "notreallyhashed"
    db_user = users.User(email=user.email,
                         password=fake_hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_items(db: SessionLocal, skip: int = 0, limit: int = 0):
    """Read items.

    Args:
        db: db session
        skip: skiped records.
        limit: maxium read records.

    Returns:
        A quey result.

    Raises:
        None
    """
    db.query(users.Item).offset(skip).limit(limit).all()


def create_user_item(db: SessionLocal, item: ItemCreate, user_id: int):
    """Creat user item.

    rgs:
        db: db session
        item:  
        user_id: 

    Returns:
        A object of new created item.

    Raises:
        None
    """
    db_item = users.Item(**item.dict(), owner_id=user_id)
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item
