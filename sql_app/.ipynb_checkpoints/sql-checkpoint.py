#!./venv/bin/python 
"""sql main for postgresql database.

Filename: sql.py
Author: George Pang
Contact: panglilimcse@139.com
"""

import os
import sys
from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from sqlalchemy.orm import Session
print(sys.path)
if __name__ == '__main__':
    sys.path.append(os.getcwd())
    sys.path.append(os.path.dirname(os.getcwd()))
    import crud
    import models
    import schemas
    from database import SessionLocal
    from database import engine
else:
    from . import crud
    from . import models
    from . import schemas
    from .database import SessionLocal
    from .database import engine

print(sys.path)
print('sql.py==',__package__, __name__)
# print("SessionLocal is {}".format(SessionLocal))


models.Base.metadata.create_all(bind=engine)

router = APIRouter()

# Dependecy
def get_db(): 
    """Function summary.

    Args:
        db: db session
        table_name: name of the table to stock codes

    Returns:
        A list of stock share code,which in our watching list ect.

    Raises:
        None
    """
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.post("stock/plate-list")
def save_plate_list(
    plate_list: schemas.PlateList, 
    db: Session = Depends(get_db)
):
    """Save list of plates.

    Args:
        db: db session
        plate_list: a list of plates in a market

    Returns:
        None

    Raises:
        None
    """
    data = {
        "table_name": models.PlateList, 
        "values": plate_list, 
        "index": ['code']
    }
            
    crud.save_data_db(data)


@router.post("/stock/baseinfo/")
def create_base_info(
    base_info: schemas.BaseInfo,
    db: Session = Depends(get_db)
): 
    """Save basic information of a stock by code.

    Args:
        db: db session
        base_info: basic information of a stock with specific code

    Returns:
        None

    Raises:
        None
    """
    data = {
        "table_name": models.BaseInfo, 
        "values": base_info, 
        "index": ['code']
    }
    crud.save_data_db(data)

@router.post("/stock/plate-stocks/", response_model=schemas.PlateStocks)
def create_plate_stocks(
    plate_stocks: schemas.PlateStocks,
    db: Session = Depends(get_db)
): 
    """Save stock in a plate.

    Args:
        db: db session
        plate_stocks: stocks in a plate

    Returns:
        None

    Raises:
        None
    """
    data = {
        "table_name": models.PlateStocks, 
        "values": plate_stocks, 
        "index": ['code']
    }
    crud.save_data_db(data)


@router.post("stock/k-day/")
def save_k_day(klines: schemas.K_DAY, db: Session = Depends(get_db)):
    """Save k line data.

    Args:
        db: db session
        klines: list of k line data of stock codes

    Returns:
        None

    Raises:
        None
    """
    data = {
        "table_name": models.K_DAY, 
        "values": klines, 
        "index": [
            "code",
            "time_key"
        ]
    }
    crud.save_data_db(data)


@router.post("/stock/history-kline/", response_model=schemas.HistoryKline)
def create_history_kline(
    history_kline: schemas.HistoryKline, 
    kline_type: str = 'K_DAY',
    update: bool = False,
    db: Session = Depends(get_db) 
): 
    """Save k line data by type.

    Args:
        db: db session
        history_kline: list of k line data
        kline_type: k line type
        update: only update if this is true

    Returns:
        None

    Raises:
        None
    """
    data = {
        "table_name": kline_type, 
        "values": history_kline, 
        "index": ["code", "time_key"]
    }
           
    crud.save_history_kline(data, update)


#@router.get("/stock/history-kline", response_model=list[schemas.HistoryKline])
@router.get("/stock/history-kline")
def read_history_kline(code: str, kltype: str, db: Session = Depends(get_db)): 
    """Read history k line.

    Args:
        db: db session
        kltype: type of the data toed
        code: code of a stock

    Returns:
        A list of history k line data for a stock by type.

    Raises:
        None
    """
    #db_history_klines = crud.get_history_kline(SessionLocal(), code, kltype)
    db_history_klines = crud.get_history_kline(SessionLocal(), code, kltype)

    if db_history_klines is None:
        raise HTTPException(status_code=400, detail="Klines not found")

    result = list(map(lambda x: x.to_dict(), db_history_klines))
#    print(f'from sql.py, result of read_history_kline: {result[:2]}')
    return result


@router.get("/stock/code-list")
def read_code_list(kltype: str='K_DAY'):
    """Read code list by kline type.

    Args:
        db: db session
        kltype: string of a kline type

    Returns:
        A list of stock share codes for a k line type in the database.

    Raises:
        None
    """
    return crud.get_code_list(SessionLocal(), kltype)

@router.post("/users/", response_model=schemas.User)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    """Save user.

    Args:
        db: db session
        user: data of a user

    Returns:
        None

    Raises:
       HTTPException 
    """
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(statys_code=400, detail="Email already registered")
    return crud.create_user(db=db, user=user)


@router.get("/users/", response_model=list[schemas.User])
def read_users(skip: int = 0, limit: int = 100, db:Session = Depends(get_db)):
    """Read users.

    Args:
        db: db session
        skip: number of records to skip off   
        limit: number of records to list in a page

    Returns:
        A list of suer data

    Raises:
        None
    """
    users = crud.get_users(db, skip=skip, limit=limit)
    return users


@router.get("/users/{user_id}", response_model=schemas.User)
def read_user(user_id: int, db: Session = Depends(get_db)):
    """Read a user.

    Args:
        db: db session
        user_id: id of a user

    Returns:
        A list of user data

    Raises:
       HTTPException 
    """
    db_user = crud.get_user(db, user_id)
    if db_user is None:
        raise HTTPException(status_code=400, detail="User not found")
    return db_user


@router.post("/users/{user_id}/items/", response_model=schemas.Item)
def create_item_for_user(
    user_id: int, 
    item: schemas.ItemCreate, 
    db: Session = Depends(get_db)
):
    """Save user item.

    Args:
        db: db session
        user_id: id of user
        item: item data 

    Returns:
        None

    Raises:
        None
    """
    return crud.create_user_item(db=db, item=item, user_id=user_id)


@router.get("/items/", response_model=list[schemas.Item])
def read_items(
    skip: int = 0, 
    limit: int = 100, 
    db: Session = Depends(get_db)
):
    """Read items.

    Args:
        db: db session
        skip: number of records to skip off
        limit: maxium record of read

    Returns:
        None

    Raises:
        None
    """
    items = crud.get_items(db, skip=skip, limit=limit)
    return items


@router.get("/stock/plate-stocks/")
def get_plate_stocks(db: Session = Depends(get_db)): 
    """Read stocks by plate.

    Args:
        db: db session

    Returns:
        None

    Raises:
        None
    """
    print('read_plate_stocks'.center(40,'-'))
    plate_stocks = crud.get_data_db(
        SessionLocal(), 
        {"model_name": models.PlateStocks, 
         "filter": []}
    )
                                        
    return plate_stocks


@router.get("/stock/base-info/", response_model=schemas.BaseInfo)
def get_base_info(code: str, db: Session = Depends(get_db)):
    """Get basic information of a stock code .

    Args:
        db: db session
        code: code of a stock

    Returns:
        A object of stock basic information.

    Raises:
        None
    """
    print('read_base_info'.center(40,'-'))
    base_info = crud.get_data_db(
        SessionLocal(), 
        {
            "model_name": models.BaseInfo,
            "filter": code
        }
    )

    return base_info 
    

@router.get("/codes/")
def get_codes_from_history_klines( 
    db: Session=Depends(get_db), 
    table_name: str='K_YEAR' 
):
    """Get codes from get_codes_from_history_klines.

    Args:
        db: db session
        table_name: name of the table to stock codes

    Returns:
        A list of stock share code,which in our watching list ect.

    Raises:
        None
    """
    print('get_codes_from_history_klines'.center(60, '='))
    return crud.get_codes(SessionLocal(), table_name)


@router.get("/code_name/")
def get_code_name(codes: list):
    """Get shtock code name.

    Args:
        codes: list of stock codes

    Returns:
        A list of objects, typeed as {"code": string, "name": string}

    Raises:
        None
    """
    print('get-code-name'.rjust(60,'-'))
    map(lambda code: code['code'], codes)
    stocks = list(map(lambda code: crud.get_basic_info(
        SessionLocal(),
        code['code']),
            codes)
    )
    stocks_filtered = list(filter(
        lambda stock: stock is not None, 
        stocks)
    )
    result = map(lambda item:  {"code": item.code, "name":
                                item.name},stocks_filtered)
   
    return list(result)


if  __name__ == '__main__':

    result=get_codes_from_history_klines()
#    result = read_history_kline('HK.01919', 'K_DAY', SessionLocal())
    print(result)      
    exit(0)

    print('sql'.center(60,'-'))
    filter_data = ['HK.00001', 'HK.00007', 'HK.00028', 'HK.00050', 'HK.00081', 
                   'HK.00084',
     'HK.00114', 'HK.00124', 'HK.00127', 'HK.00129', 'HK.00142', 'HK.00154',
     'HK.00166', 'HK.00201', 'HK.00219', 'HK.00222', 'HK.00240', 'HK.00253',
     'HK.00259', 'HK.00267', 'HK.00311', 'HK.00382', 'HK.00390', 'HK.00392',
     'HK.00408', 'HK.00434', 'HK.00483', 'HK.00505', 'HK.00576', 'HK.00579',
     'HK.00598', 'HK.00608', 'HK.00807', 'HK.00811', 'HK.00819', 'HK.00833',
     'HK.00841', 'HK.00842', 'HK.00848', 'HK.00857', 'HK.00882', 'HK.00883',
     'HK.00889', 'HK.00922', 'HK.00939', 'HK.00945', 'HK.00954', 'HK.00975',
     'HK.00998', 'HK.01001', 'HK.01023', 'HK.01065', 'HK.01100', 'HK.01120',
     'HK.01123', 'HK.01126', 'HK.01127', 'HK.01152', 'HK.01186', 'HK.01203',
     'HK.01205', 'HK.01245', 'HK.01273', 'HK.01277', 'HK.01288', 'HK.01289',
     'HK.01330', 'HK.01336', 'HK.01339', 'HK.01346', 'HK.01362', 'HK.01397',
     'HK.01398', 'HK.01419', 'HK.01428', 'HK.01449', 'HK.01455', 'HK.01480',
     'HK.01522', 'HK.01526', 'HK.01556', 'HK.01568', 'HK.01582', 'HK.01599',
     'HK.01601', 'HK.01606', 'HK.01618', 'HK.01627', 'HK.01643', 'HK.01651',
     'HK.01658', 'HK.01669', 'HK.01705', 'HK.01713', 'HK.01730', 'HK.01749',
     'HK.01798', 'HK.01800', 'HK.01900', 'HK.01905']
    
    data = list(map(lambda code: { "code": code}, filter_data))
    print('result'.rjust(60, '-'))
    print(data)

    result = get_code_name(data[:2])
    print('result'.rjust(60, '-'))
    print(result)

    exit()

    print(get_codes_from_history_klines(SessionLocal(), 'K_YEAR'))

    exit(0)


    print(get_db)

    result = read_code_list()
    print(result)
    
    exit()

    #codes = ['HK.06806', 'SH.600854']
    code = 'HK.01919'
    result = get_base_info(code)
#    result = get_plate_stocks()
    print(result)
    print(result.code)
    print(result.name)
    print(dir(result))
    print(result.__dict__)
    
    exit()
    from database.database import SessionLocal



