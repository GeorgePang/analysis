#!./venv/bin/python 
"""Crud operations.

Filename: crus.py
Author: George Pang
Contact: panglilimcse@139.com
"""

from sqlalchemy.orm import Session
from sqlalchemy.dialects.postgresql import insert

print(f'crud.py, {__package__}, {__name__}')
if __package__ is None or __package__ == '':
    import models 
    import history_klines 
    import schemas 
    import database 
else:
    from . import database, schemas, models, history_klines

def get_table_class(table_name: str):
    """Get class by k line type.

    Args:
        table_name: name of the table to stock codes.

    Returns:
        A class of history k line type for stock share.

    Raises:
        None
    """
    result = database.Base 
    result = history_klines.K_DAY if table_name =='K_DAY' else result
    result = history_klines.K_1M if table_name == 'K_1M' else result 
    result = history_klines.K_3M if table_name == 'K_3M' else result
    result = history_klines.K_5M if table_name == 'K_5M' else result
    result = history_klines.K_15M if table_name == 'K_15M' else result 
    result = history_klines.K_30M if table_name == 'K_30M' else result 
    result = history_klines.K_60M if table_name == 'K_60M' else result 
    result = history_klines.K_WEEK if table_name == 'K_WEEK' else result
    result = history_klines.K_MON if  table_name == 'K_MON' else result
    result = history_klines.K_QUARTER if table_name == 'K_QUARTER' else result
    result = history_klines.K_YEAR if table_name == 'K_YEAR' else result

    return result


def get_data_db(db: Session, data: any):
    """Read data from db.

    Args:
        db: db session
        data:  dict like {"module_name": string, "fi;ter": string}

    Returns:
        A query result.

    Raisee:
        None
    """
    result = database.base 
    result = db.query(data['model_name'])
    result = result.filter(data['model_name'].code == data['filter'])
    return result.first() 


def save_data_db(data, update=True):
    """Save data to db.

    Args:
        data: db data.
        update: boolean, if true ,using update method whane conflict. 

    Returns:
        None

    Raises:
        None
    """
    insert_stmt = insert(data['table_name']).values(data['values'])
    if update:
        do_update_stmt = insert_stmt.on_conflict_do_update(
            index_elements=data['index'],
            set_=dict(
                k= insert_stmt.excluded.k,
                d=insert_stmt.excluded.d, 
                j=insert_stmt.excluded.j, 
                dif=insert_stmt.excluded.dif, 
                dea=insert_stmt.excluded.dea, 
                macd=insert_stmt.excluded.macd
            )
        )
                                                  
        with database.engine.connect() as connection:
            connection.execute(do_update_stmt)
            connection.commit()
        return

    do_nothing_stmt = insert_stmt.on_conflict_do_nothing(
        index_elements=data['index']
    )
    with database.engine.connect() as connection:
        connection.execute(do_nothing_stmt)
        connection.commit()
    return


def create_history_kline(
    db: Session, 
    history_kline_data: schemas.HistoryKline
):
    """Insert history kline.

    Args:
        db: db session.
        history_kline_data : data of the history kliness.

    Returns:
        None

    Raises:
        None
    """
    print(history_kline_data)

    insert_stmt = insert(models.HistoryKline).values(history_kline_data)

    do_nothing_stmt = insert_stmt.on_conflict_do_nothing(
        index_elements=[
            'code', 
            'time_key'
        ]
    )

    with database.engine.connect() as connection:
        connection.execute(do_nothing_stmt)
        connection.commit()
    return

def get_history_kline(
    db: Session, 
    code: str, 
    kltype: str='K_DAY'
):
    """Read history klines.

    Args:
        db: db session.
        code: code of the stock.
        kltype: type

    Returne:
        A query result.

    Raises:
        None
    """
    db_table = get_table_class(kltype)
    kline = db.query(db_table).filter(
        db_table.code==code
    ).order_by(
        db_table.time_key
    ).all()

    return  kline


def save_history_kline(data, update=True ):
    """Save history k lines.

    Args:
        data: data  
        update: boolean, update data if it is true when confilict

    Returns:
        None

    aises:
        None
    """
    insert_stmt = insert(get_table_class(data['table_name'])).values(data['values'])
    if update:
        do_update_stmt = insert_stmt.on_conflict_do_update(
            index_elements=data['index'],
            set_=dict(
                k= insert_stmt.excluded.k,
                d=insert_stmt.excluded.d, 
                j=insert_stmt.excluded.j, 
                dif=insert_stmt.excluded.dif, 
                dea=insert_stmt.excluded.dea, 
                macd=insert_stmt.excluded.macd
            )
        )
        with database.engine.connect() as connection:
            connection.execute(do_update_stmt)
            connection.commit()
        return
    else:
        do_nothing_stmt = insert_stmt.on_conflict_do_nothing(
            index_elements=['code', 'time_key']
        )

        with database.engine.connect() as connection:
            connection.execute(do_nothing_stmt)
            connection.commit()
        return



def get_code_list(db: Session, db_table = history_klines.K_DAY  ):
    """Read all stock codes.

    Args:
        db: db session
        db_table: table class of the stock codes by type

    Returns:
        A list of stock share code,which in our watching list ect.

    Raises:
        None
    """
    codes = db.query(db_table.code).distinct().all()
    return list(map(lambda code: (code[0], code[1]), codes))


def get_user(db: Session, user_id: int):
    """Read user.

    Args:
        db: db session.
        user_id: id of the user.

    Returns:
        A list of user information

    Raises:
        None
    """
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_email(db: Session, email: str):
    """Read use by email.

    Args:
        db: db session
        email: user email.

    Returns:
        A list of user information.

    Raises:
        None
    """
    return db.query(models.User).filter(models.User.email == email ).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    """Read user list.

    Args:
        db: db session
        skip: skiled recourds.
        limit: maxium read records.

    Returns:
        A list of user information.

    Raises:
        None
    """
    return db.query(models.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserCreate):
    """Create a new user.

    Args:
        db: db session
        user: user create schema

    Returns:
        A object of created user.

    Raises:
        None
    """
    fake_hashed_password = user.password + "notreallyhashed"
    db_user = models.User(email=user.email,
                          hashed_password=fake_hashed_password)
    db.add(db_user) 
    db.commit()
    db.refresh(db_user)
    return db_user


def get_items(db: Session, skip: int = 0, limit: int = 0):
    """Read items.

    Args:
        db: db session
        skip: skiped records.
        limit: maxium read records.

    Returns:
        A quey result.

    Raises:
        None
    """
    db.query(models.Item).offset(skip).limit(limit).all()

def create_user_item(db: Session, item: schemas.ItemCreate, user_id: int):
    """Creat user item.

    rgs:
        db: db session
        item:  
        user_id: 

    Returns:
        A object of new created item.

    Raises:
        None
    """
    db_item = models.Item(**item.dict(), owner_id=user_id)
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item


def get_codes(db :Session, tablename: str):
    """Function summary.

    Args:
        db: db session
        tablename: name of the table to stock codes

    Returns:
        A list of stock share code,which in our watching list ect.

    Raises:
        None
    """
    table = get_table_class(tablename)
    result =  db.query(table.code, table.name).distinct().all()
    return list(map(lambda x: (x[0], x[1]), result))


def get_basic_info(db: Session, code: str):
    """Function summary.

    Args:
        db: db session
        code: stock code

    Returns:
        A query result of basic information for a stock share idendified by code.

    Raises:
        None
    """
    result = db.query(models.BaseInfo).filter(models.BaseInfo.code==code).first()
    return result                                      


if __name__ == '__main__':

    result = get_basic_info(database.SessionLocal(), 'HK.00001')
    print(result)
    print(dir(result))
    print(result.__dict__)
    print(dict(result.__dict__))

    exit(0)
    
