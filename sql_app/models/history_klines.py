#!./venv/bin/python 
"""History k lime tables for postgresql database.

Filename: hitory_klines.py
Author: George Pang
Contact: panglilimcse@139.com
"""

from sqlalchemy import Column, Float, String
from sql_app.database import Base

class Kline(Base):
    """Common model of kilines for diferrent type."""
    __abstract__ = True

    code = Column(String, primary_key=True, index=True, comment='股票代码')
    name = Column(String, comment='股票名称')
    time_key = Column(String, primary_key=True, index=True, comment='K线时间')
    open = Column(Float, comment='开盘价 ')
    close = Column(Float, comment='收盘价')
    high = Column(Float, comment='最高价')
    low = Column(Float, comment='最低价')
    pe_ratio = Column(Float, comment='市盈率')
    turnover_rate = Column(Float, comment='换手率')
    volume = Column(Float, comment='成交量')
    turnover = Column(Float, comment='成交额')
    change_rate = Column(Float, comment='涨跌幅')
    last_close = Column(Float, comment='昨收价')
    k = Column(Float, server_default='0', comment='K')
    d = Column(Float, server_default='0', comment='D')
    j = Column(Float, server_default='0', comment='J')
    dif = Column(Float,  server_default='0', comment='DIF')
    dea = Column(Float,  server_default='0', comment='DEA')
    macd = Column(Float, server_default='0',  comment='MACD')

    # 把SQLAlchemy查询对象转换成字典
    def to_dict(self):
        """Change SQLAlchemy query to dict.

        Args:
            self: self object

        Returns:
            A dict of attributes in query result
        Raises:
            None
        """    
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class K_DAY(Kline):
    """Daily klines."""
    __tablename__ = 'k_day'



class K_60M(Kline):
    """Sixty minutes klines."""
    __tablename__ = 'k_60m'


class K_WEEK(Kline):
    """Weekly klines."""
    __tablename__ = 'k_week'


class K_MON(Kline):
    """Monthly klines."""
    __tablename__ = 'k_mon'


class K_YEAR(Kline): 
    """Yearly klines."""
    __tablename__ = 'k_year'


class K_QUARTER(Kline):
    """Quarterly klines."""
    __tablename__ = 'k_quarter'


#class K_1M(Kline):
#    """One minute klines."""
#    __tablename__ = 'k_1m'
#
#
#class K_3M(Kline):
#    """Three minutes klines."""
#    __tablename__ = 'k_3m'
#
#
#class K_5M(Kline):
#    """Five minutes klines."""
#    __tablename__ = 'k_5m'
#
#
#class K_15M(Kline):
#    """Fifteen minutes klines."""
#    __tablename__ = 'k_15m'
#
#
#class K_30M(Kline):
#    """Thirty minutes klines."""
#    __tablename__ = 'k_30m'
#
