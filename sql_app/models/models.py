#!./venv/bin/python 
"""sql models for postgresql database.

Filename: models.py
Author: George Pang
Contact: panglilimcse@139.com
"""

from sqlalchemy import Boolean, Column, Float, Integer, String, UniqueConstraint
from sqlalchemy.dialects.postgresql import BIGINT

from sql_app.commons import KLineType_enum
from sql_app.commons import Oscillation_enum
from sql_app.commons import Tendency_enum
from sql_app.commons import Pattern_enum
from sql_app.commons import market_type_enum
from sql_app.commons import plate_type_enum
from sql_app.commons import stock_child_type_enum
from sql_app.commons import exchange_type_enum
from sql_app.commons import stock_type_enum
from sql_app.commons import option_type_enum
from sql_app.database import Base

 
class PlateList(Base):
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    __tablename__ = "plate_list"

    code = Column(String, primary_key=True, index=True, comment="板块代码")
    plate_name = Column(String, comment="板块名字")
    plate_id = Column(String, comment="板块ID")
    market = Column(market_type_enum, comment='交易市场')
    plate_class = Column(plate_type_enum, comment='板块类别')


class PlateStocks(Base):
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    __tablename__ = "plate_stocks"

    code = Column(String, primary_key=True, index=True, comment="股票代码")
    lot_size = Column(Integer, comment="每手股票，期货表示合约乘数")
    stock_name = Column(String, index=True, comment="股票名称")
    stock_type = Column(stock_type_enum, comment="股票类型")
    stock_child_type = Column(stock_child_type_enum, comment='窝论子类型')
    stock_owner = Column(String, comment='窝论所属正股的代码')
    list_time = Column(String, comment="上市时间")
    stock_id = Column(BIGINT, comment="股票ID")
    main_contract = Column(Boolean, comment="是否主连合约")
    last_trade_time = Column(String, comment="最后交易时间")


class BaseInfo(Base):
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    __tablename__ = "baseinfo"

    code = Column(String, primary_key=True, index=True, comment="股票代码")
    name = Column(String, index=True, comment="股票名称")
    listing_date = Column(String, comment="上市时间")
    lot_size = Column(Integer, comment="每手股数") 
    stock_type = Column(stock_type_enum,  comment='股票类型')
    stock_child_type = Column(stock_child_type_enum, comment='窝论子类型')
    stock_owner = Column(String, comment='窝论所属正股的代码')
    option_type = Column(option_type_enum, comment='期权类型')
    strike_time = Column(String, comment='期权行权日')
    strike_price = Column(Float, comment='期权行权价')
    suspension = Column(String, comment='期权是否停牌')
    stock_id = Column(BIGINT, comment='股票ID')
    delisting = Column(Boolean, comment='是否退市')
    index_option_type = Column(String, comment='指数期权类型')
    main_contract = Column(Boolean, comment='是否主连合约')
    last_trade_time = Column(String, comment='最后交易时间')
    exchange_type =  Column(exchange_type_enum, comment='所属交易所')


class Conclusion(Base):
    """Conclusion of techical ideces for a stock by cyclong type.

    Stocks are  identified by code.
    With timestamp and  cycling type from k line types.

    Attributes:
        code: A code for stock, like 'HK.01919'.
        name: Name of th stock.
        timestump: timestump when this conclusion is made, 
            it is also the last k lines' timestamp when conculusion is made.
        kind: Cycling type of indeces.
        oscillation: oscillation result, calculated from KDJ.
        tendency: Trend result, from MACD.
        pattern: k line pattern, from Pattern_enum.
        context: A descriptive statement of the stock.
    """
    __tablename__ = 'conclusion'
    __table_args__ = (
        UniqueConstraint('code', 'name', 'time_key', 'kind', name='conc'),
        )

    code = Column(String, primary_key=True, index=True, comment="股票代码")
    name = Column(String, primary_key=True,comment="股票名称")
    time_key = Column(String, primary_key=True, comment="时间戳")
    kind = Column(KLineType_enum, primary_key=True, comment="周期")
    oscillation = Column(Oscillation_enum, comment='摆动指标')
    tendency = Column(Tendency_enum, comment="趋势指标")
    pattern    = Column(Pattern_enum, comment="K线模式")
    context = Column(String, comment="结论内容", default='SH.00001')


