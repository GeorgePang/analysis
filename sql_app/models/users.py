#!./venv/bin/python 
"""Common args, function, and classes.

Filename: users.py
Author: George Pang
Contact: panglilimcse@139.com
"""

from sqlalchemy import Boolean, Column, Enum, Float, ForeignKey, \
    Integer, String, UniqueConstraint
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import ARRAY
from sql_app.database import Base

sex = ('male', 'female')
sex_enum = Enum(*sex, name='sex_enum', create_type=False)


invoice_status = ('pending', 'paid')
invoice_status_enum = Enum(*invoice_status,
                           name='invoice_status_enum',
                           create_type=False
                           )


class User(Base):  
    """user class."""
    __tablename__ = "users"

    id = Column(String, primary_key=True, index=True)
    name = Column(String, index=True)
    email = Column(String, unique=True, index=True)
    password = Column(String)
    is_active = Column(Boolean, default=True)
    sex = Column(sex_enum, comment='性别')
    collection = Column(ARRAY(String))

    items = relationship("Item", back_populates="owner")


class Customer(Base):  
    """customer class.

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    __tablename__ = "customers"

    id = Column(String, primary_key=True, index=True)
    name = Column(String, index=True)
    email = Column(String, unique=True, index=True)
    image_url = Column(String)


class Invoice(Base):  
    """invoicde class.

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    __tablename__ = "invoices"

    id = Column(Integer,  autoincrement=True, primary_key=True, index=True)
    customer_id = Column(String, index=True)
    amount_in_cents = Column(Float, index=True)
    date = Column(String, index=True)
    status = Column(invoice_status_enum, index=True)
    uix_1 = UniqueConstraint('customer_id', 'amount_in_cents', 'date', 'status')


class Revenue(Base):  
    """Revenue class.

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    __tablename__ = "revenue"

    month = Column(String, primary_key=True)
    revenue = Column(Float)


class Item(Base):
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    __tablename__ = "items"

    id = Column(String, primary_key=True, index=True)
    title = Column(String, index=True)
    description = Column(String, index=True)
    owner_id = Column(String, ForeignKey("users.id"))
    sex = Column(sex_enum)

    owner = relationship("User", back_populates="items")

