#!./venv/bin/python
"""This module is sql operations.

Leave one blank line.  The rest of this docstring should contain an
overall description of the module or program.  Optionally, it may also
contain a brief description of exported classes and functions and/or usage
examples.

Typical usage example:

      foo = ClassFoo()
        bar = foo.FunctionBar()
        

The sql.py is main program, which input and output data with python dataformat,
like dict, list etc.
All other progam should communicate each other througn padanic
data object, eg. sql query result.
"""
import os
import sys
sys.path.append(os.getcwd())
sys.path.append(os.path.dirname(os.getcwd()))
