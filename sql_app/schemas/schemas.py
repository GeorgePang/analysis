#!./venv/bin/python 
"""schema of data for pydantic.

Filename: schemas.py
Author: George Pang
Contact: panglilimcse@139.com
"""

from pydantic import BaseModel


class ItemBase(BaseModel):
    """Base class of item.

    Longer class information...
    Longer class information...

    Attributes:
        title:  item title.
        description: description of title
        sex: sex
    """
    title: str
    description: str | None = None
    sex: str


class ItemCreate(ItemBase):
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    pass


class Item(ItemBase):
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        id: item id.
        owner_id: id of item owner.
    """
    id: int
    owner_id: int

    class Config:
        """Summary of class here.

        Longer class information...
        Longer class information...

        Attributes:
            likes_spam: A boolean indicating if we like SPAM or not.
            eggs: An integer count of the eggs we have laid.
        """
        from_attributes = True


class UserBase(BaseModel):
    """Base user.

    Attributes:
        email: email address.
        sex: sex.
    """
    email: str
    sex: str


class UserCreate(UserBase):
    """For creating a user.

    This class is designed to createa new user, it hasn't a id column.

    Attributes:
        password: hashed password string.
    """
    password: str


class User(UserBase):
    """The user class.

    Attributes:
        id: ithe unique identification number.
        is_active: boolean, whether this use is active.
        items: list, items belong to this user.
    """
    id: str
    is_active: bool
    items: list[Item] = []
    collection: list[str] = []

    class Config:
        """Configrations."""
        from_attributes = True


class Customer(BaseModel):
    """customer class.

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    id: str
    namem: str
    email: str
    image_url: str


class Invoice(BaseModel):
    """invoicde class.

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    id: int
    customer_id: str
    amount_in_cents: float
    date: str
    status: str


class Revenue(BaseModel):
    """invoicde class.

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    month: str
    revenue: float


class PlateList(BaseModel):
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    code: str
    plate_name: str
    plate_id: str
    market: str
    plate_class: str


class PlateStocks(BaseModel):
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    code: str
    lot_size: int
    stock_name: str
    stock_type: str
    stock_child_type: str
    stock_owner: str
    list_time: str
    stock_id: int
    main_contract: bool
    lat_trade_time: str

    class Config:
        """Summary of class here.

        Longer class information...
        Longer class information...

        Attributes:
            likes_spam: A boolean indicating if we like SPAM or not.
            eggs: An integer count of the eggs we have laid.
        """
        from_attributes = True


class BaseInfo(BaseModel):
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """
    code: str
    name: str
    listing_date: str
    lot_size: int
    stock_type: str
    stock_child_type: str
    stock_owner: str
    option_type: str
    strike_time: str
    strike_price: float
    suspension: str
    stock_id: int
    delisting: bool
    index_option_type: str
    main_contract: bool
    last_trade_time: str
    exchange_type: str

    class Config:
        """Summary of class here.

        Longer class information...
        Longer class information...

        Attributes:
            likes_spam: A boolean indicating if we like SPAM or not.
            eggs: An integer count of the eggs we have laid.
        """
        from_attributes = True


class HistoryKline(BaseModel):
    """Pydantic schema of klines.

    All fields relative to a sqlalchemy model column with the dame name.

    """
    code: str
    name: str
    time_key: str
    open: float
    high: float
    low: float
    close: float
    pe_ratio: float
    turnover_rate: float
    volume: int
    turnover: float
    change_rate: float
    last_close: float
    k: float
    d: float
    j: float
    dif: float
    dea: float
    macd: float

    class Config:
        """Subclass for configuration.

        Longer class information...
        Longer class information...

        """
        from_attributes = True


class K_DAY(HistoryKline):
    """Daily kiline schema."""


class Conclusion(BaseModel):
    """Conclusion of a stock for a serials of cycling type.

    Stocks are identified by code, with timestamp and cycling type.

    Attributes:
        code: A code for stock.
        name: Name of stock.
        timestamp: timestamp when this conclusion issued, e.g. whith which k line.
        kind: Cycling type of indeces.
        oscillation: oscillation result, calculated from KDJ.
        tendency: Trend result, from MACD.
        pattern: J line pattern, from pattern types Enum.
        context: A descriptive statement of stock.
    """
    code: str
    name: str
    time_key: str
    kind: str
    oscillation: str
    tendency: str
    pattern: str
    context: str

    class Config:
        """Summary of class here.

        Longer class information...
        Longer class information...

        Attributes:
            likes_spam: A boolean indicating if we like SPAM or not.
            eggs: An integer count of the eggs we have laid.
        """
        from_attributes = True
