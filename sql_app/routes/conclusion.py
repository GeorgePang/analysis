#!/usr/bin/env python
"""API to Conclusion.

Filename: conclusion.py
Author: George Pang
Contact: panglilimcse@139.com
"""

import json
import pandas as pd
from pandas import DataFrame as df
from fastapi import APIRouter
from fastapi import Depends
from sqlalchemy.orm import Session
from commons import transform_to_datarows
from classes.commons import KLineType
from classes.MyLogger import MyLog
from routes import sql_data
from sql_app.commons import Oscillation_enum, Tendency_enum
from sql_app.routes.sql import  get_codes_from_history_klines
from sql_app.cruds import crud, conclusion_crud
from sql_app.cruds import user_crud
# from sql_app.cruds.crud import select_filtered_conclusions
from sql_app.database import get_db, SessionLocal, shape_data
from sql_app.models import models

router = APIRouter(
    prefix="/conclusion",
    tags=["conclusion"],
    #    dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Not found"}},
)


def validating(value: str, collection: list):
    """Validate a values is in collection."""
    if value in collection:
        return True
    return False


@router.get("/")
def reads():
    """Test for this api.

    Return a string to show this api is work or not.
    String keys will be UTF-8 encoded.

    Args:
       None.

    Returns:
        A string = "conclusion from data of sql_api".

    Raises:
        IOError: An error occurred accessing the api.
    """
    return "conclusion from data of sql_api"


@router.get("/{code}/{kind}")
def read_conclusion(code: str, kind: str = 'K_DAY',
                    db: Session = Depends(get_db)):
    """Fetches conclusion from Conclusion table by code or dtime.

    get the conclusion by code or dtime.
    String keys will be UTF-8 encoded.

    Args:
        db: db session
        code: A string of code for stock.
        kind: A string of k line kind.  String keys will be UTF-8 encoded.
        
    Returns:
        A list of conclusions. 
        Each conclusion is a dict mapping keys to the corresponding table row data
        fetched. 

        None while that row was not found in the table.

    Raises:
        Non
    """

    conclusion = crud.get_data_db(
        db,
        {
            "model_name": models.Conclusion,
            "filter": code,
            "filters": [kind]
        }
    ).order_by(models.Conclusion.time_key.desc())

    return shape_data(conclusion.all())


@router.get("/lastConclusions/")
async def get_last_conclusions(user_id: str, limit: int = 5, db: Session = Depends(get_db)):
    """Fetch last limit of conclusions in tuple."""

    print(f'conclusion.py to get last conclusions'.rjust(80,'-'))

    user = await user_crud.get_user_by_id(user_id, db)
    codes = user.collection
    print(f'in conclusion.py get_last_conclusions,  codes is: {codes}.')

    result = df(crud.get_last_conclusions(tuple(codes), limit, db))

    return result.to_dict('records')


def save_multi_conclusions(codes: list = [], kinds: list = []):
    """Save multi conclusions.

    This function save multi kinds and multi codes conclusions.

    Args:
        codes: list of stock codes.
        kinds: list od k line types.

    Return:
        exit()

    Raises:
        None

    """
    codes = get_codes_from_history_klines() if codes == [] else codes
    kinds = KLineType if kinds == [] else kinds

    print(f'codes is {codes}'.rjust(80, '-'))
    print(f'kinds is {kinds}'.rjust(80, '-'))

    for kind in kinds:
        if kind == '':
            continue

        for code in codes:
            save_conclusion(code[0], kind)

    exit('finish save multi conclusions'.rjust(80, '*'))


def save_conclusion(code: str, kind: str = 'K_DAY'):
    """Saves rows to conclusion table.

    Calculate the trading signals and compose then into conclusion by code and datetime.
    Save these conclusions as rows into the Conclusion table.
    String keys will be UTF-8 encoded.

    Args:
        code: A string of code for stock.
        kind: A string of data cycle type.

    Returns:
        A dict mapping keys to the meta data of conclusions saved:

        {code: str,
         timestamp: str,
         rows_saved: int}
    """
    print(f'save conclusion: {code}, {kind}'.rjust(80, '-'))
    data = sql_data.read_history_kline_sql(code, kind)
    if data is None:
        return

    d = df(data['values'], columns=data['columns']).sort_values(by='time_key')
    oscillations = calculate_oscillations(d.iloc[:, [2, 13, 14, 15]])
    tendency = calculate_tendency(d.iloc[:, [2, 16, 17, 18]])

    conclusion = pd.merge(oscillations, tendency, how='outer')
    result_head = d.iloc[:, :3]
    result_head['kind'] = kind
    result = pd.merge(
        result_head,
        conclusion,
        how='right',
        left_on='time_key',
        right_on='time_key'
    )
    result.ffill(inplace=True)
    result.bfill(inplace=True)
    result['pattern'] = ''
    result.fillna('', inplace=True)
    print(result)
    result['context'] = result['oscillation'] \
                        + ' ' + result['tendency'] \
                        + ' ' + result['pattern']

    result.dropna(inplace=True)

    if result.empty:
        print('empty!!'.rjust(80, '*'))
        return

    result.fillna(' ', inplace=True)

    return conclusion_crud.save_conclusions(transform_to_datarows(result))


def calculate_oscillations(data: df):
    """Calculate the oscillations from a stock's kdj.

    cacluate oscillation signals from the kdj data.
    
    Args:
        data: a pandas dataframe. Columns are: 
            ['time_key', 'k', 'd', 'j']. where:
            
            time_key: datetime string, like '2013-12-11 00:00:00'.
            k: k value float.
            d: d value float.
            j: j valoe float.
            
    Returns:
        A pandas dataframe or None. The pandsa dataframe has columns are like:
           ['time_key', 'oscillation'],where:
           time_key: string, the timestamp.
           oscillation: string, one of the is the item in list:
               ['UP_CROSS', 'UP_BOTTOM', 'DOWN_CROSS', 'DOWN+TOP'].
        
    Raises:
        None
    """
    result = []
    d = data.dropna()

    '''UP_CROSS'''
    d1 = d.loc[(d['k'] > d['d']) & (d['k'].shift() < d['d'].shift())].copy()
    d1['oscillation'] = Oscillation_enum.enums[1]
    d1 = d1.iloc[:, [0, -1]]

    '''UP_BOTTOM'''
    d0 = d.loc[(d['j'] > 0) & (d['j'].shift() < 0)].copy()
    d0['oscillation'] = Oscillation_enum.enums[0]
    d0 = d0.iloc[:, [0, -1]]

    '''DOWN_CROSS'''
    d3 = d.loc[(d['k'] < d['d']) & (d['k'].shift() > d['d'].shift())].copy()
    d3['oscillation'] = Oscillation_enum.enums[3]
    d3 = d3.iloc[:, [0, -1]]

    '''DOWN+TOP'''
    d2 = d.loc[(d['j'] < 100) & (d['j'].shift() > 100)].copy()
    d2['oscillation'] = Oscillation_enum.enums[3]
    d2 = d2.iloc[:, [0, -1]]

    result = pd.concat([d0, d1, d2, d3]).sort_values(by='time_key')

    return result


def calculate_tendency(data: df):
    """Calculate a srock's tendency from its macd.

    cacluate tendency signals from the macd data.
    
    Args:
        data: pandas DataFrame, columns are:
            [time_key, dif,dea, macd], where:
            time_key: string, timestamp.
            dif: float.
            dea: float.
            macd: float.

    Returns:
        pandas DataFrame or None.
        columns od DataFrame are: 
            ['time_key', 'tendency'], where:
            time_key: string, timestamp.
            tendency: string, one of the items in list:
          
    Raises:
        None
    """
    d = data.dropna()

    '''LONG'''
    d2 = d.loc[d['macd'] > 0].copy()
    d2['tendency'] = Tendency_enum.enums[2]
    d2 = d2.iloc[:, [0, -1]]

    '''SHORT{'''
    d3 = d.loc[d['macd'] < 0].copy()
    d3['tendency'] = Tendency_enum.enums[3]
    d3 = d3.iloc[:, [0, -1]]

    '''LONG_TURN'''
    d4 = d.loc[(d['macd'] > 0) & (d['macd'].shift() < 0)].copy()
    d4['tendency'] = Tendency_enum.enums[4]
    d4 = d4.iloc[:, [0, -1]]

    '''SHORT_TURN'''
    d5 = d.loc[(d['macd'] < 0) & (d['macd'].shift() > 0)].copy()
    d5['tendency'] = Tendency_enum.enums[5]
    d5 = d5.iloc[:, [0, -1]]

    '''LONG_STRONG'''
    d0 = d.loc[(d['macd'] > 0) & (d['dif'] > 0)].copy()
    d0['tendency'] = Tendency_enum.enums[0]
    d0 = d0.iloc[:, [0, -1]]

    '''SHORT_STRONG'''
    d1 = d.loc[(d['macd'] < 0) & (d['dif'] < 0)].copy()
    d1['tendency'] = Tendency_enum.enums[1]
    d1 = d1.iloc[:, [0, -1]]

    result = pd.concat([d0, d1, d2, d3, d4, d5])
    result.drop_duplicates(subset=['time_key'], inplace=True)
    result.sort_values(by='time_key')

    return result


@router.get("/filtered_conclusions/")
async def get_filtered_conclusions(
        userid: str,
        keyword: str = '',
        skip: int = 0,
        limit: int = 5,
        whole: bool = False,
        db: SessionLocal = Depends(get_db)
):
    """Get filtered conclusion by keyword.

    ARGs:
        db:
        whole:
        userid: id of the user.
        keyword: atring, which should in conclusion's code, name, time .etc.
        skip: int, the record number from where fetching started.
        limit: int, how many records to fetch.
    Returns:
        list of dict of consclusions
    """
    user = await user_crud.get_user_by_id(userid, db)
    print(f'in conclusions.py get_filtered_conclusion, user is {user}')
    print(f'in conclusions.py get_filtered_conclusion, collections is {user.collection}')
    codes = tuple(user.collection)

    result = await crud.select_filtered_conclusions(codes, db, keyword, skip, limit, whole)
    print(f'result is {result}')
    result = df(result).to_dict('records')

    return result


@router.get("/conclusion_pages/")
async def get_conclusion_pages(userid: str, keyword: str = '', db: SessionLocal=Depends(get_db)):
    """Fetches rows from a Smalltable.

    Args:
        db:
        userid: the id of user to query.
        keyword: string for keyword in or related to invoices.

    Returns:
        number of pages
    """
    user = await user_crud.get_user_by_id(userid, db)
    print(f'in conclusions.py/conclusion_pages(), user is {user}'.rjust(80,'-'))
    codes = tuple(user.collection)
    result = await conclusion_crud.read_conclusion_pages(codes, keyword, db)
    return result


