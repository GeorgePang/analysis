#!./venv/bin/python 
"""API for users.

Filename: users.py
Author: George Pang
Contact: panglilimcse@139.com
"""

from fastapi import APIRouter
from fastapi.params import Depends

from sql_app.cruds.user_crud import get_user_by_id, get_user_by_name, get_user_by_email
from sql_app.database import get_db, SessionLocal

router = APIRouter()


@router.get("/userid/{userid}", tags=["users"])
async def get_userid(userid: str):
    """Fetches user by id.

    Args:
        userid: string of userid.

    Returns:
        dict of user.
    """
    return get_user_by_id(userid)


@router.get("/username/{username}", tags=["users"])
async def get_user(username: str, db: SessionLocal = Depends(get_db)):
    """Fetches user by name.

    Args:
        db:
        username: string of username.

    Returns:
        dict of user.
    """
    return get_user_by_name(username, db)


@router.get("/usermail/{email}", tags=["users"])
async def get_user_email(email: str, db:SessionLocal = Depends(get_db)):
    """Fetches user by email from table.

    Args:
        db:
        email: user email

    Returns:
        dict of user.
    """
    return get_user_by_email(email, db)

