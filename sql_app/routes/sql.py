#!./venv/bin/python 
"""sql main for postgresql database.

Filename: sql.py
Author: George Pang
Contact: panglilimcse@139.com
"""

from fastapi import APIRouter
from fastapi import Depends
from pandas import DataFrame as dataframe
from sqlalchemy import text
from sqlalchemy.orm import Session

from sql_app.cruds import crud
from sql_app.database import get_db, SessionLocal
from sql_app.models import models
from sql_app.schemas import schemas

router = APIRouter()


@router.post("/stock/plate-list")
def save_plate_list(
        plate_list: schemas.PlateList,
        db: SessionLocal = Depends(get_db)
):
    """Save list of plates.

    Args:
        db: db session
        plate_list: a list of plates in a market

    Returns:
        None

    Raises:
        None
    """
    data = {
        "table": models.PlateList,
        "values": plate_list,
        "index": ['code']
    }

    crud.save_data_db(data, db)


@router.post("/stock/baseinfo/")
def create_base_info(
        base_info: schemas.BaseInfo,
        db: SessionLocal = Depends(get_db)
):
    """Save basic information of a stock by code.

    Args:
        db: db session
        base_info: basic information of a stock with specific code

    Returns:
        None

    Raises:
        None
    """
    data = {
        "table": models.BaseInfo,
        "values": base_info,
        "index": ['code']
    }
    crud.save_data_db(data, db)


@router.post("/stock/plate-stocks/", response_model=schemas.PlateStocks)
def create_plate_stocks(
        plate_stocks: schemas.PlateStocks,
        db: SessionLocal = Depends(get_db)
):
    """Save stock in a plate.

    Args:
        db: db session
        plate_stocks: stocks in a plate

    Returns:
        None

    Raises:
        None
    """
    data = {
        "table": models.PlateStocks,
        "values": plate_stocks,
        "index": ['code']
    }
    crud.save_data_db(db, data)


def read_revenue(skip: int = 0, limit: int = 100, db: Session = Depends(get_db())):
    """Read revenue.

    Args:
        db: db session
        skip: number of records to skip off   
        limit: number of records to list in a page

    Returns:
        A list of revenu data

    Raises:
        None
    """
    revenue = crud.get_revenue(db, skip=skip, limit=limit)
    return list(map(lambda x: {"month": x.month, "revenue": x.revenue}, revenue))


def read_counts(table_name: str):
    """Fetch count(*) from table_name."""
    result = crud.get_counts(table_name)

    return dataframe(result).to_dict('records')


@router.get("/items/", response_model=list[schemas.Item])
def read_items(
        skip: int = 0,
        limit: int = 100,
        db: Session = Depends(get_db)
):
    """Read items.

    Args:
        db: db session
        skip: number of records to skip off
        limit: maxium record of read

    Returns:
        None

    Raises:
        None
    """
    items = crud.get_items(db, skip=skip, limit=limit)
    return items


@router.get("/stock/plate-stocks-pages/")
def get_plate_stocks_pages(query: str = '', db: Session = Depends(get_db)):
    """Read stocks by plate.

    Args:
        query:
        db: db session

    Returns:
        list of stocks from plates
    """
    stmt_all = '''SELECT COUNT(DISTINCT code)
        FROM plate_stocks
        '''
    stmt_query = '''SELECT COUNT(DISTINCT code)
            FROM plate_stocks
            WHERE
              plate_stocks.code LIKE :query OR
              plate_stocks.stock_name LIKE :query 
        '''

    stmt = stmt_all if query == '' else stmt_query

    result = db.execute(
        text(stmt),
        {
            'query': '%' + query + '%',
        }
    ).fetchall()

    print(result[0][0])

    return result[0][0]


@router.get("/stocks/code-name")
def get_code_name_from_plate_stocks(query: str = '', skip: int = 0, limit: int = 5, db: SessionLocal = Depends(get_db)):
    stmt_all = '''SELECT DISTINCT code, stock_name
    FROM plate_stocks
    OFFSET :skip
    LIMIT :limit
    '''
    stmt_query = '''SELECT DISTINCT code, stock_name
        FROM plate_stocks
        WHERE
          plate_stocks.code LIKE :query OR
          plate_stocks.stock_name LIKE :query 
        OFFSET :skip
        LIMIT :limit
    '''

    stmt = stmt_all if query == '' else stmt_query

    result = db.execute(
        text(stmt),
        {
            'query': '%' + query + '%',
            'skip': skip,
            'limit': limit
        }
    ).fetchall()

    print(len(result))

    result = set(result)

    print(len(result))

    result = list(map(lambda item: {"code": item[0], "name": item[1]}, result))
    print(result[:10])
    return result


@router.get("/stock/base-info/", response_model=schemas.BaseInfo)
async def get_base_info(code: str, db: Session = Depends(get_db)):
    """Get basic information of a stock code .

    Args:
        db: db session
        code: code of a stock

    Returns:
        A object of stock basic information.

    Raises:
        None
    """
    print('read_base_info'.center(40, '-'))
    base_info = await crud.get_basic_info(code, db)

    return base_info.to_dict()


@router.get("/codes/")
def get_codes_from_history_klines(
        table_name: str = 'K_YEAR',
        db: SessionLocal = Depends(get_db)
):
    """Get codes from get_codes_from_history_klines.

    Args:
        db: db session
        table_name: name of the table to stock codes

    Returns:
        A list of stock share code,which in our watching list ect.

    Raises:
        None
    """
    print('get_codes_from_history_klines'.center(60, '='))
    print(f'get_codes_from_history_klines, {db}'.center(60, '='))
    return crud.get_codes(table_name, db)


@router.get("/code_name/{code}")
async def get_code_name(code: str, db: SessionLocal = Depends(get_db)):
    """Get stock code name.

    Args:
        db:
        code: Rcode  of stock

    Returns:
            {"code": string, "name": string}

    """
    print('get-code-name'.rjust(60, '-'))

    stock = await crud.get_basic_info(code, db)
    print(stock)

    result = {"code": stock.code, "name": stock.name}

    return result
