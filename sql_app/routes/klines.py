#!./venv/bin/python 
"""k lines for postgresql database.

Filename: kline.py
Author: George Pang
Contact: panglilimcse@139.com
"""

from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from pandas import DataFrame as dataframe
from sqlalchemy.orm import Session
from sqlalchemy.testing.suite.test_reflection import users

from sql_app.cruds import crud
from sql_app.database import get_db, SessionLocal
from sql_app.models import history_klines
from sql_app.schemas import schemas

router = APIRouter()


@router.post("stock/k-day/")
def save_k_day(klines: schemas.K_DAY, db: SessionLocal = Depends(get_db)):
    """Save k line data.

    Args:
        db: db session
        klines: list of k line data of stock codes

    Returns:
        None

    Raises:
        None
    """
    data = {
        "table": history_klines.K_DAY,
        "values": klines,
        "index": [
            "code",
            "time_key"
        ]
    }
    crud.save_data_db(data, db)


@router.post("/stock/history-kline/", response_model=schemas.HistoryKline)
def create_history_kline(
        history_kline: schemas.HistoryKline,
        kline_type: str = 'K_DAY',
        update: bool = False
):
    """Save k line data by type.

    Args:
        db: db session
        history_kline: list of k line data
        kline_type: k line type
        update: only update if this is true

    Returns:
        None

    Raises:
        None
    """
    data = {
        "table": kline_type,
        "values": history_kline,
        "index": ["code", "time_key"]
    }

    crud.save_history_kline(data, update)


# @router.get("/stock/history-kline", response_model=list[schemas.HistoryKline])
@router.get("/stock/history-kline")
def read_history_kline(code: str,
                       kltype: str,
                       db: Session = Depends(get_db)
                       ):
    """Read history k line.

    Args:
        db: db session
        kltype: type of the data toed
        code: code of a stock

    Returns:
        A list of history k line data for a stock by type.

    Raises:
        None
    """
    # db_history_klines = crud.get_history_kline(SessionLocal(), code, kltype)
    db_history_klines = crud.get_history_kline(db, code, kltype)

    if db_history_klines is None:
        raise HTTPException(status_code=400, detail="Klines not found")

    result = list(map(lambda x: x.to_dict(), db_history_klines))
    #    print(f'from sql.py, result of read_history_kline: {result[:2]}')
    result.reverse()
    return result


@router.get("/stock/code-list")
def read_code_list(kltype: str = 'K_DAY',
                   db:SessionLocal = Depends(get_db)):
    """Read code list by kline type.

    Args:
        db: db session
        kltype: string of a kline type

    Returns:
        A list of stock share codes for a k line type in the database.

    Raises:
        None
    """
    return crud.get_code_list(db, kltype)




async def read_conclusion_pages(userid: str, keyword: str = ''):
    """Read invoices.

    Args:
        userid: id of user. 
        keyword: string of keyword in invoices.   

    Returns:
        A list of nvoices have keyword in or related to. 
    """
    user = users.read_userid(userid)
    codes = tuple(user['collection'])
    conclusions = crud.get_conclusion_pages(codes, keyword)
    return dataframe(conclusions).to_dict('records')[0]



@router.get("/codes/")
def get_codes_from_history_klines(
        db: SessionLocal = Depends(get_db),
        table_name: str = 'K_YEAR'
):
    """Get codes from get_codes_from_history_klines.

    Args:
        db: db session
        table_name: name of the table to stock codes

    Returns:
        A list of stock share code,which in our watching list ect.

    Raises:
        None
    """
    print('get_codes_from_history_klines'.center(60, '='))
    return crud.get_codes(db, table_name)


@router.get("/code_name/")
def get_code_name(codes: list, db:SessionLocal = Depends(get_db)):
    """Get stock code name.

    Args:
        codes: list of stock codes

    Returns:
        A list of objects, typeed as {"code": string, "name": string}

    Raises:
        None
    """
    print('get-code-name'.rjust(60, '-'))
    map(lambda code: code['code'], codes)
    stocks = list(map(lambda code: crud.get_basic_info(
        db,
        code['code']),
                      codes)
                  )
    stocks_filtered = list(filter(
        lambda stock: stock is not None,
        stocks)
    )
    result = map(lambda item: {"code": item.code, "name":
        item.name}, stocks_filtered)

    return list(result)
