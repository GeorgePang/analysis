#!./venv/bin/python 
"""Database engine.

Filename: database.py
Author: George Pang
Contact: panglilimcse@139.com
"""

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# from dotenv import find_dotenv, load_dotenv
# load_dotenv(find_dotenv('.env'))
#
# env_dist = os.environ
# print(env_dist)

# SQLALCHEMY_DATABASE_URL = "postgresql://postgres:example@127.0.0.1:5432/db"
SQLALCHEMY_DATABASE_URL = "postgresql://postgres:PangLiLi&660507@101.35.200.235:5432/db"

engine = create_engine(SQLALCHEMY_DATABASE_URL,
                       pool_size=1000, max_overflow=5000
                       )

SessionLocal = sessionmaker(autocommit=False, autoflush=True, bind=engine)


# Base = declarative_base()


# Dependency
def get_db() -> SessionLocal:
    """Function summary.

    Returns:
        A database sessionlocal() object.

    """
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def shape_data(data: any):
    """Shape alchemy query result to list or dict.

    Args:
        data: a query result or list of query results

    Returns:
    """
    if isinstance(data, list):
        result = list(map(lambda x: x.to_dict(), data))
    else:
        result = data.to_dict()

    return result


class Base(declarative_base()):
    """Abstract base class."""
    __abstract__ = True

    # 把SQLAlchemy查询对象转换成字典
    def to_dict(self):
        """Change SQLAlchemy query to dict.

        Args:
            self: self object

        Returns:
            A dict of attributes in query result
        Raises:
            None
        """
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


if __name__ == "__main__":
    print(engine)
    print('-------------------------------------------')
    print(Base)
    print('-------------------------------------------')
    print(dir(Base))
    print('-------------------------------------------')
    print(Base.__dict__)
    print('-------------------------------------------')
    print(Base)
