from sql_app.sql import get_codes_from_history_klines
from routes.conclusion import save_conclusion

codes = get_codes_from_history_klines('K_60M')
kinds = 'K_60M' 

print(codes)
print(kinds)

for code in codes:
    save_conclusion(code[0], kinds)

exit('finish save multi conclusions'.rjust(80, '*')) 

